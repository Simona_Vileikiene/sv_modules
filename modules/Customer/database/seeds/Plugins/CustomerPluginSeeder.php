<?php

use Illuminate\Database\Seeder;

class CustomerPluginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plugins')->insert([
            'name' => 'Klientai',
            'slug' => 'Customer',
            'category_id' => 3,
        ]);
        DB::table('categories')->insert([
            'id' => 3,
            'name' => 'Klientai',
            'slug' => 'klientai'
        ]);
    }
}
