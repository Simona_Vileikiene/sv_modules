<?php

namespace Tests\Unit;

use App\Category;
use App\Http\Modules\Helpers\TimeIntervalValidation;
use Tests\TestCase;

use App\CustomerHelper;

class CustomerTest extends TestCase
{
    /**
     * @test
     */
    function getModuleCategoryPageTest()
    {
        $expectedName = 'Atsijungti';
        $expected = '/super/logout';

      //  $timeIntervalValidation = \Mockery::mock(Category::class);
      //  $timeIntervalValidation->shouldReceive('isTimeInBusinessHours')->andReturn($isTimeInBusinessHours);

        $rootCategory = \Mockery::mock(Category::class);
        $rootCategory->shouldReceive('setAttribute')->with('name')
            ->andReturn($expectedName);
        $rootCategory->shouldReceive('setAttribute')->with('category_link')
            ->andReturn($expected);
        $class = new CustomerHelper();
        $page = $class->getModuleCategoryPage($rootCategory, 'logout');
        $this->assertEquals($expectedName, $page->name);
        $this->assertEquals($expected, $page->category_link);
    }


}