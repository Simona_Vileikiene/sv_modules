@extends('admin/layout')

@section('content')

	<h1 class="col-sm-2 text-xs-center">Vartotojai</h1>
	{!! Form::open(array('method' => 'get', 'action' => 'CustomerController@index', 'class'=>'text-center col-sm-8 item-new')) !!}
		<div class="col-sm-5 col-xs-8">
			{!! Form::text('search', Request::input('search'), ['class'=>'form-control', 'placeholder'=>'Vardas, El. paštas']) !!}
		</div>
		<div class="col-sm-3 col-xs-3 pl-0 text-left">
			{!! Form::submit('Paieška', ['class'=>'btn btn-info']) !!}
			@if(Request::input('search')!='')
				<a href="{{ action("CustomerController@index") }}" class="btn btn-default">Išvalyti</a>
			@endif
		</div>
	{!! Form::close() !!}
	<div class="col-sm-2 text-right item-news">
		<a href="{{ action("CustomerController@create") }}" class="btn btn-success pull-right">
			<i class="glyphicon glyphicon-plus"></i> Sukurti naują
		</a>
	</div>
	<div class="clearfix"></div><br />

	<div class="table-responsive">
	<table class="table table-hover">
		<tr>
			<th>#</th>
			<th>Vardas</th>
	        <th>El. paštas</th>
	        <th width="200"></th>
	    </tr>
		@foreach($items as $item)

		  	<tr>
		  		<td>{{ $item->id }}</td>
		  		<td>{{ $item->name }}</td>
		  		<td>{{ $item->email }}</td>
		     	<td>
					<a href="{{ action("CustomerController@edit", $item->id) }}" class="btn btn-info" title="Redaguoti"><i class="glyphicon glyphicon-pencil"></i></a>

						{!! Form::model($item, ['method' => 'DELETE', 'action' => ['CustomerController@destroy', $item->id], 'class'=>'del-form', 'onclick'=>"return confirm('Ar tikrai norite ištrinti?')", 'title'=>'Ištrinti']) !!}
							{!! Form::submit(' X ', ['class'=>'btn btn-danger']) !!}
						{!! Form::close() !!}

		    	</td>
		    </tr>

		@endforeach
	</table>
	</div>

	{!! $items->appends(['search' => Request::input('search')])->render() !!}
@endsection 