@extends('admin/layout')

@section('content')

	<div class="col-md-6 col-md-offset-3">
		<h1>Sukurti naują</h1>
		@include('errors/list') 
		{!! Form::open(array('action' => ['CustomerController@store'], 'files' => true)) !!}
			@include('admin/customers/form')
		{!! Form::close() !!}
	</div>
	
@endsection