<br />
{!! Form::label('Vardas', '', ['class'=>'control-label']) !!}
{!! Form::text('name', @$name, ['class'=>'form-control']) !!}<br />

{!! Form::label('El. paštas*', '', ['class'=>'control-label']) !!}
{!! Form::email('email', @$email, ['class'=>'form-control']) !!}<br />


@if(isset($updatePassword))
<h3>Naujas slaptažodis</h3>
@endif

{!! Form::label('Slaptažodis*', '', ['class'=>'control-label',
'data-comment'=>(isset($updatePassword))?' (Jei norima keisti)':'']) !!}
{!! Form::password('password', ['class'=>'form-control']) !!}<br />

{!! Form::label('Pakartoti Slaptažodį', '', ['class'=>'control-label']) !!}
{!! Form::password('password2', ['class'=>'form-control']) !!}<br />

{!! Form::submit('Siųsti', ['class'=>'btn btn-info btn-lg']) !!}
