@extends('site.layout')

@section('content')


		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-default">
				<div class="panel-body">
					<h1>{{ $page->title }}</h1>
					<a href="{{ $customerPages['logout']->category_link }}">{{ $customerPages['logout']->name }}</a>
					<br />

					@if (Session::get('message'))
						<p class="alert alert-success">{{ Session::get('message') }}</p> 
					@endif
						@if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
						{!! Form::model($item, ['action' => ['Frontend\CustomerController@profileStore', $item->id]]) !!}

							<?php $new_passowrd = true; ?>
							@include('admin/customers/form')

						{!! Form::close() !!}
						</div></div>

			
		</div>

	
@endsection 