@extends('site.layout')

@section('content')


		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-default">
				<div class="panel-body">
					<h1>Pamiršau slaptažodį</h1>
					@if (Session::get('message'))
						<p class="alert alert-success">{{ Session::get('message') }}</p>
						<p><a href="{{ $customerPages['index']->category_link }}">Prisijungti</a></p>
					@else
						@if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
						{!! Form::open(array('action' => 'Frontend\CustomerController@reminderStore')) !!}


							{!! Form::label('El. paštas*', '', ['class'=>'control-label']) !!}
							{!! Form::email('email', @$email, ['class'=>'form-control']) !!}<br />

							{!! Form::submit('Siųsti', ['class'=>'btn btn-info']) !!}

						{!! Form::close() !!}
						</div></div>
					@endif
			
		</div>

	
@endsection 