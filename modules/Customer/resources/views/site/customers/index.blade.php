@extends('site.layout')

@section('content')

<h1>{{ $page->title }}</h1>

<div class="col-md-6 col-md-offset-3">
	<h2>Prisijungti</h2>
	@if (Session::get('error'))
		<p class="alert alert-danger">{{ Session::get('error') }}</p> 
	@endif
	@include('errors/list') 
	{!! Form::open(array('action' => 'Frontend\CustomerController@loginStore')) !!}
		
		{!! Form::label('El. paštas', '', ['class'=>'control-label']) !!}
		{!! Form::email('email', @$email, ['class'=>'form-control']) !!}<br />
		                            
		{!! Form::label('Slaptažodis', '', ['class'=>'control-label']) !!}
		{!! Form::password('password', ['class'=>'form-control']) !!}<br />

		{!! Form::submit('Prisijungti', ['class'=>'btn btn-info']) !!}

		<br /><br />
		<a href="{{ $customerPages['forgot']->category_link }}">{{ $customerPages['forgot']->name }}</a> |
		<a href="{{ $customerPages['register']->category_link }}">{{ $customerPages['register']->name }}</a>
	{!! Form::close() !!}
</div>
<div class="clearfix"></div>

@endsection 