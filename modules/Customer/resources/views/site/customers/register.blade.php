@extends('site.layout')

@section('content')


		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-default">
				<div class="panel-body">
					<h1>Registracija</h1>
					@if (Session::get('message'))
						<p class="alert alert-success">{{ Session::get('message') }}</p> 
					@else
						@if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
						{!! Form::open(array('action' => 'Frontend\CustomerController@registerStore')) !!}

							@include('admin/customers/form')

						{!! Form::close() !!}
						</div></div>
					@endif
			
		</div>
	
@endsection 