<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
use Request;

class CustomerRequest extends FormRequest
{
        /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = 0;
        if ($this->method()=='PATCH') { //if admin update
            $id = Request::segment(3);
        } elseif (Auth::guard('customer')->check()) { //if affl update
            $id = Auth::guard('customer')->user()->id;
        }
        $rules = [
            'password' => 'same:password2',
        ];
        //rules update
        if ($id>0) {
            $rules['email'] = 'required|email|unique:customers,email,'.$id;
        } else {
            //rules if create
            $rules['password'] = 'required|same:password2';
            $rules['email'] = 'required|email|unique:customers,email';
        }
        return $rules;
    }
}