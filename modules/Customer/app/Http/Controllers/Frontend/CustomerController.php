<?php

namespace App\Http\Controllers\Frontend;

use App;
use App\Http\Requests\ResetRequest;
use Auth;
use Hash;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Password;
use Mail;

use App\Customer;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\CustomerRequest;
use App\Http\Requests\CustomerForgotRequest;


class CustomerController extends App\Http\Controllers\Controller
{
    use ThrottlesLogins;

    public function username() {
        return 'email';
    }
    public function loginStore(LoginRequest $request)
    {
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }
        if (Auth::guard('customer')->attempt([
            'email' => $request->input("email"), 'password' => $request->input("password")
        ], true))
        {
            $this->clearLoginAttempts($request);
            return redirect()->back();
        }
        $this->incrementLoginAttempts($request);
        return redirect()->back()->withInput()->with('error', 'Blogi prisijungimo duomenys');
    }

    public function registerStore(CustomerRequest $request)
    {
        $item = Customer::create($request->all());
        $item->update(["password" => Hash::make($request->input("password"))]);
        $data = ["email" => $item->email, 'password' => $request->input("password")];
        $email = $item->email;
        $name = $item->name;
        Mail::send('emails.register', $data, function($message) use ($email, $name)
        {
            $message->to($email, $name)->subject('Registracija');
        });
        Auth::guard('customer')->loginUsingId($item->id);
        return redirect()->back()->with('message', 'OK');
    }

    //site. profile
    public function profileStore(CustomerRequest $request)
    {
        $item = Auth::guard('customer')->user();
        if($item) {
            $item->update($request->all());
            if ($request->input("password") != "") {
                $item->password = Hash::make($request->input("password"));
                $item->save();
            }
            return redirect()->back()->with('message', 'Informacija atnaujinta');
        }
        return redirect('/');
    }

    public function reminderStore(CustomerForgotRequest $request)
    {
        $item = Customer::where("email", $request->input("email"))->first();
        if ($item) {
            $customerHelper = new App\CustomerHelper();
            $customerPages = $customerHelper->getModuleCategories();
            $token = Password::broker('customers')->createToken($item);
            $link = url($customerPages['reset']->category_link . '?token=' . $token . '&email=' . $item->email);
            $data = ['link' => $link];
            $email = $item->email;
            $name = $item->name;
            Mail::send('emails.forgot', $data, function($message) use ($email, $name)
            {
                $message->to($email, $name)->subject('Pamiršau slaptažodį');
            });
            return redirect()->back()->with('message', 'Laikas išsiųstas.');
        }
        return view('site.customers.forgot');
    }

    public function resetStore(ResetRequest $request)
    {
        $data['reset_token'] = $request->input('reset_token');
        $user = Customer::where('email', $request->input('email'))->first();
        if($user) {
            if (!Password::broker('customers')->tokenExists($user, $data['reset_token'])) {
                return redirect()->back()->with('error', $data['error']);
            }
        } else {
            return redirect()->back()->with('error', 'Tokio el. pašto nėra');
        }
        $user->password = Hash::make($request->input("password"));
        $user->save();
        return redirect()->back()->with('message', 'Slaptažodis sėkmingai pakeistas.');
    }
}