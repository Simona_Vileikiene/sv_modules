<?php

namespace App\Http\Controllers;

use Mail;
use Hash;
use Auth;
use Request;

use App\Customer;
use App\Http\Requests\CustomerRequest;

class CustomerController extends Controller
{
    //admin. list
    public function index()
    {
        $items = Customer::where("id", ">", 0);
        if (Request::input('search') != "") {
            $items = $items->where("email", "like", "%" . Request::input('search') . "%")
                ->orwhere("name", "like", "%" . Request::input('search') . "%");
        }
        $items = $items->orderby('created_at', 'DESC')->paginate(50);
        return view('admin/customers/index', compact("items"));
    }

    //admin. create new
    public function create()
    {
        return view('admin/customers/create');
    }

    //admin. save create
    public function store(CustomerRequest $request)
    {
        $item = Customer::create($request->all());
        $item->update(["password" => Hash::make($request->input("password"))]);
        return redirect()->action('CustomerController@index');
    }

    //admin. edit
    public function edit($id)
    {
        $item = Customer::findorFail($id);
        $updatePassword = true;
        return view('admin/customers/edit', compact('item', 'updatePassword'));
    }

    //admin. save edit
    public function update($id, CustomerRequest $request)
    {
        $item = Customer::findorFail($id);
        $item->update($request->all());
        if ($request->input("password") != "") {
            $item->password = Hash::make($request->input("password"));
            $item->save();
        }
        return redirect()->action('CustomerController@index');
    }

    //admin. destroy
    public function destroy($id)
    {
        $item = Customer::findorFail($id);
        $item->delete();
        return redirect()->action('CustomerController@index');
    }
}
