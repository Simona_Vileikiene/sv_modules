<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
    //no guarded, cause multi lang has unnecessary fields
    protected $fillable = ['name', 'email'];

    const SITE_CATEGORY_ID = 3;

    const MODULE_CATEGORIES = [
        'index' => [
            'name' => 'Klientas', 'slug' => ''
        ],
        'register' => [
            'name' => 'Registruotis', 'slug' => 'registracija'
        ],
        'forgot' => [
            'name' => 'Pamiršau slaptažodį', 'slug' => 'pamirsau'
        ],
        'logout' => [
            'name' => 'Atsijungti', 'slug' => 'atsijungti'
        ],
        'reset' => [
            'name' => 'Slaptažodžio keitimas', 'slug' => 'pakeisti'
        ]
    ];
}

class CustomerHelper
{
    public function getModuleCategories() {
        $items = [];
        foreach(Customer::MODULE_CATEGORIES as $categoryId => $item) {
            $items[$categoryId] = $this->getModuleCategoryPage($categoryId);
        }
        return $items;
    }

    public function getModuleCategoryPage($categoryId) {
        $moduleCategories = Customer::MODULE_CATEGORIES;
        $item = Category::find(Customer::SITE_CATEGORY_ID)->translate();
        $item->category_id = $categoryId;
        if($categoryId == 'index') {
            $customerPage = Category::find(Customer::SITE_CATEGORY_ID)->translate();
            $item->name = $customerPage->name;
        } else {
            $item->name = $moduleCategories[$categoryId]['name'];
        }
        $item->category_link = $item->link.'/'.$moduleCategories[$categoryId]['slug'];
        return $item;
    }


    public function getModuleCurrent($request = false)
    {
        $moduleCategories = Customer::MODULE_CATEGORIES;
        $segments = ($request)?$request->segments():\Request::segments();
        $slug = last($segments);
        foreach ($moduleCategories as $category => $item) {
            if($slug == $item['slug']) {
                return $this->getModuleCategoryPage($category);
            }
        }
        return $this->getModuleCategoryPage('index');
    }
}