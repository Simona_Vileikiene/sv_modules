<?php

use Illuminate\Database\Seeder;

class ShoppingCartPluginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plugins')->insert([
            'name' => 'Prekių krepšelis',
            'slug' => 'shoppingCart',
            'category_id' => 12
        ]);
        DB::table('categories')->insert([
            'id' => 12,
            'name' => 'Prekių krepšelis',
            'slug' => 'krepselis'
        ]);

        DB::table('settings')->insert([
            'id' => 3,
            'key' => 'cartEmpty',
            'name' => 'Krepšelio klaida. Krepšelis tuščias',
            'body' => 'Krepšelis tuščias'
        ]);
        DB::table('settings')->insert([
            'id' => 4,
            'key' => 'cartMaxError',
            'name' => 'Krepšelio klaida. Maksimalus šio produkto kiekis yra',
            'body' => 'Maksimalus šio produkto kiekis yra'
        ]);
    }
}
