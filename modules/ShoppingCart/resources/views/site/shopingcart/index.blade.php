@extends('site.layout')

@section('content')
    <h1 class="h1">{{ $page->name  }}</h1>

    @if(count($items)>0)
        @if (Session::get('cartMaxError'))
            <p class="alert alert-danger">{{ trans('db.cartMaxError').Session::get('cartMaxError') }}</p>
        @endif

        <div id="ajax-cart-view">
            <table class="table table-hover">
                <tr>
                    <th>Nuotrauka</th>
                    <th>Pavadinmas</th>
                    <th class="col-md-1">Kaina</th>
                    <th class="col-md-1">Kiekis</th>
                    <th class="col-md-1">Viso</th>
                </tr>
                @foreach($items as $id=>$item)
                    <?php $product = App\Product::find($item->id); ?>
                    @if($product)
                        <tr>
                            <td class="thumbnail" style="width:150px;">@if($product->img=="")<img
                                        src="/site/images/blank.jpg"
                                        alt="{{ $item->name }}"/> @else{!! $product->img  !!} @endif</td>
                            <td><a href="{{ $product->link }}" class="title">{{ $item->name  }}

                                    {{ $item->options->combination['name'] }}</a></td>
                            <td>{{ $item->price  }} Eur</td>
                            <td>
                                {!! Form::text('qty-'.$item->id, $item->qty, ['class'=>'form-control']) !!}
                                <a href="{{ action("Frontend\ShoppingCartController@update", $id) }}"
                                   data-name="shoppingCartUpdate"
                                   data-id="{{ $item->id }}"
                                   class="alert-success ajax-cart-update">
                                    <i class="glyphicon glyphicon-floppy-disk"></i></a>
                                <a href="{{ action("Frontend\ShoppingCartController@destroy", $id) }}"
                                   class="alert-danger ajax-cart-destroy"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                            <td class="ajax-cart-subtotal-{{ $id }}"><span>{{ $item->subtotal  }}</span> Eur</td>
                        </tr>
                    @endif
                @endforeach
            </table>

            <table class="table table-hover">
                @if($deliverMethods && $deliverMethods->count()>0  && Cart::total()>0)
                    <tr>
                        <td class="col-md-10"></td>
                        <td class="text-right col-md-1" style="font-weight: bold">Viso:</td>
                        <td class="col-md-1"><span data-name="ajax-cart-subtotal">{{ Cart::total() }}</span> Eur</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="text-right" style="font-weight: bold">Pristatymas:</td>
                        <td><span data-name="ajax-cart-delivery">0.00</span> Eur</td>
                    </tr>
                @endif
                <tr>
                    <td></td>
                    <td class="text-right" style="font-weight: bold">Viso mokėti:</td>
                    <td style="font-weight: bold"><span data-name="ajax-cart-total">{{ Cart::total() }}</span> Eur</td>
                </tr>
            </table>
            <div class="clearfix"></div>
            <br/>

            @if($deliverMethods && $deliverMethods->count()>0  && Cart::total()>0)
                @include('resources.views.errors.list')
                {!! Form::open(array('action' => ['Frontend\ShoppingCartController@chooseDelivery'], 'files' => true)) !!}
                {!! Form::label('Pristatymas', '', ['class'=>'control-label']) !!}<br/>
                @foreach($deliverMethods->get() as $item)
                    {!! Form::radio('delivery_id', $item->id, @$delivery_id,
                    ['class'=>'radio', 'data-delivery-price'=>$item->price, 'id' => 'delivery_id-'.$item->id]) !!}
                    {!! Form::label('delivery_id-'.$item->id, $item->name.' ('.$item->price.' Eur') !!}<br/>
                @endforeach
                <br/>
                {!! Form::submit('Toliau', ['class'=>'btn btn-info btn-lg']) !!}
                {!! Form::close() !!}
            @else
                <a href="{{ $orderPage->link }}" class="btn btn-info btn-lg">Toliau</a>
            @endif

        </div>
    @else
        <div class="alert alert-danger"> Krepšelis tuščias</div>
    @endif
@endsection

@section('footer')
    <script>
        function deliveryPrice() {
            var delivery = $('[data-delivery-price]:checked').attr('data-delivery-price')*1;
            $('[data-name="ajax-cart-delivery"]').text(delivery.toFixed(2));
            var subtotal = $('[data-name="ajax-cart-subtotal"]').text()*1;
            var total = delivery+subtotal;
            $('[data-name="ajax-cart-total"]').text(total.toFixed(2));
        }
        $('[data-delivery-price]').click(function () {
            deliveryPrice();
        });
        $('[data-name="shoppingCartUpdate"]').click(function () {
            var id = $(this).attr('data-id');
            var qty2 = $('input[name="qty-' + id + '"]').val();
            var link = $(this).attr('href') + '?qty=' + qty2;
            location.href = link;
            return false;
        });
    </script>
@endsection