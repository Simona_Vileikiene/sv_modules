<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests\OrderDeliveryRequest;
use \Cart;
use Request;

use App\Category;
use App\Product;
use App\ProductsCombination;

class ShoppingCartController extends \App\Http\Controllers\Controller
{

    //add product to cart
    public function add($productId = null, $combinationId = null, $qty =0)
    {
        $item = Product::find($productId);
        if($item) {
            $combination = ProductsCombination::find($combinationId);
            if($combination->product_id != $item->id) {
                return redirect()->back()->with('cartError', 'Cart error');
            }
            if(!$combination) {
                $combination = $item->combinations()->first();
            }
            Cart::add($item->id, $item->name, $qty, $combination->priceTrue,
                ['combination' => ['id' => $combination->id, 'name' => $combination->name]]);
            //Determine that the amount of the basket is lees then in the warehouse
            if(Product::USE_WAREHOUSE) {
                $cartQtyByCombination = $this->getCartQtyByCombinationId($item->id, $combination->id);
                if (!empty($cartQtyByCombination) && $combination->qty < $cartQtyByCombination['qty']) {
                    Cart::update($cartQtyByCombination['id'], $combination->qty);
                }
            }
            return redirect()->back()->with('cartMessage', 'Cart OK');
        }
        return redirect()->back()->with('cartError', 'Cart error');
    }

    //user. update cart
    public function update($id)
    {
        $subtotal = 0;
        if(Cart::get($id) and is_numeric(Request::input('qty'))) {
            $cartItem = Cart::get($id);
            $qty = Request::input('qty');
            //Determine that the amount of the basket is lees then in the warehouse
            if(Product::USE_WAREHOUSE) {
                $productCombination = Product::find($cartItem->id)->combinations()->where('id', $cartItem->options->combination['id'])->first();
                if($productCombination && $productCombination->qty < $qty) {
                    $qty = $productCombination->qty;
                    Cart::update($id, $qty);
                    return redirect()->back()->with('cartMaxError', $qty);
                }
            }
            Cart::update($id, $qty);
        }
        return redirect()->back();
    }

    //user. delete from cart
    public function destroy($id)
    {
        if(Cart::get($id)) {
            Cart::remove($id);
        }
        return redirect()->back();
    }


    //save delivery from cart
    public function chooseDelivery(OrderDeliveryRequest $request)
    {
        $request->session()->put('cart_delivery_id', $request->input("delivery_id"));
        return redirect(action('Frontend\OrderController@index'));
    }

    //get product's combination qty in cart
    private function getCartQtyByCombinationId($productId, $combinationId) {
        foreach (Cart::content() as $id => $cartItem) {
            if($cartItem->id == $productId && $cartItem->options->combination['id'] == $combinationId) {
                return ['id' => $id, 'qty' => $cartItem->qty];
            }
        }
        return null;
    }

    public function getInfo() {
        return ['count' => Cart::count(), 'total' => Cart::total()];
    }
}