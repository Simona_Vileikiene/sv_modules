<?php 

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use File;
use Request;
use Cart;

class Discount extends Model
{
    //no guarded, cause multi lang has unnecessary fields
    protected $fillable = ['name', 'code', 'type', 'size', 'active', 'active_from', 'active_to'];

    protected function getIsActiveNowAttribute()
    {
        $now = Carbon::now()->format('Y-m-d') ;
        if($this->active == 1) {
            if(($this->active_from < $now && $this->active_to > $now) || empty($this->active_from)) {
                return TRUE;
            }
        }
        return FALSE;
    }
}

class DiscountHelper {

    function getDiscountSize($type, $price)
    {
        if($type==1) {
            //get only product without discount
            $size = 0;
            foreach(Cart::content() as $cartItem) {
                $product = Product::find($cartItem->id);
                if($product->oldprice <=  $product->trueprice) {
                    $size += $cartItem->price * $cartItem->qty * $price / 100;
                }
            }
        } else {
            $size = $price;
        }

        return $size;
    }
}