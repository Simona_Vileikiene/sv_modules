<?php 

namespace App\Http\Controllers; 

use App\Priority;
use App\TranHelper;
use Request;
use App\Discount;
use App\Tran;
use App\Image;
use Illuminate\Validation\Validator;
use App\Http\Requests\DiscountRequest;
use Response;

use Illuminate\Support\Str;

class DiscountController extends Controller
{
    public function __construct()
    {
        $this->tranHelper = new TranHelper();
    }

    //admin. list
    public function index()
    {
        $items = Discount::paginate(50);
       	return view('admin/discount/index', compact("items"));
    }

    //admin. create new
    public function create($id=0)
    {
        return view('admin/discount/create', compact('id'));
    }

    //admin. save create
    public function store(DiscountRequest $request)
    {
        $item = Discount::create($request->all());
        return redirect()->action('DiscountController@index');
    }

    //admin. edit
    public function edit($id)
    {
        $item = Discount::findorFail($id);
        return view('admin/discount/edit', compact('item'));
    }

    //admin. save edit
    public function update($id, DiscountRequest $request)
    {
        $item = Discount::findorFail($id);
        $item->update($request->all());
        $item->update(["active" => ($request->input("active")==1)?1:0]);
        return redirect()->action('DiscountController@index');
    }

    //admin. destroy
    public function destroy($id)
    {
        $item = Discount::findorFail($id);
        $item->delete();
        return redirect()->action('DiscountController@index');
    }

    //no used, but laravel should has it for resource route
    public function show()
    {
        exit();
    }

}
