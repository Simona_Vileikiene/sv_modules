@extends('admin/layout')

@section('content')

    <h1 class="pull-left">Discount</h1>
    <a href="{{ action("DiscountController@create") }}" class="btn btn-success pull-right">
        <i class="glyphicon glyphicon-plus"></i> {{ trans('admin.create_new') }}
    </a>
    <div class="clearfix"></div><br/>

    <div class="table-responsive">
        <table class="table table-hover ">
            <tr>
            <tr>
                <th>#</th>
                <th>Pavadinimas</th>
                <th>Kodas</th>
                <th>Dydis</th>
                <th>Aktyvus</th>
                <th width="150">
                </th>
            </tr>
            @foreach($items as $item)

                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->code }}</td>
                    <td>{{ $item->size }} @if($item->type==1)% @else Eur @endif</td>
                    <td>
                        @if($item->active == 1)
                            @if(!empty($item->active_from))
                                nuo {{ $item->active_from }} iki  {{ $item->active_to }}
                            @else
                                Aktyvus
                            @endif
                        @else
                            Neaktyvus
                        @endif
                    </td>
                    <td>
                        <a href="{{ action("DiscountController@edit", $item->id) }}" class="btn btn-info"
                           title="{{ trans('admin.edit') }}"><i
                                    class="glyphicon glyphicon-pencil"></i></a>
                        {!! Form::model($item, ['method' => 'DELETE', 'action' => ['DiscountController@destroy', $item->id], 'class'=>'del-form', 'onclick'=>"return confirm('".trans('admin.remove_confirm')."')", 'title'=>trans('admin.remove')]) !!}
                        {!! Form::submit(' X ', ['class'=>'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>

            @endforeach
        </table>
    </div>
    {!! $items->appends(['search' => Request::input('search')])->render() !!}
@endsection 