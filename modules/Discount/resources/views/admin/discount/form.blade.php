@include('admin/langs')

{!! Form::checkbox('active', 1, @$active, ['class'=>'form-control radio']) !!}
{!! Form::label('Aktyvus', '', ['class'=>'control-label']) !!}
<br/>

{!! Form::label('Pavadinimas*', '', ['class'=>'control-label']) !!}
{!! Form::text('name', @$name, ['class'=>'form-control multi']) !!}<br />

{!! Form::label('Kodas*', '', ['class'=>'control-label']) !!}
{!! Form::text('code', @$code, ['class'=>'form-control multi']) !!}<br />

{!! Form::label('Tipas*', '', ['class'=>'control-label']) !!}
{!! Form::select('type', ['Pinigai', 'Procentai'], @$type, ['class'=>'form-control']) !!}<br />

{!! Form::label('Dydis*', '', ['class'=>'control-label']) !!}
{!! Form::text('size', @$size, ['class'=>'form-control']) !!}<br />

{!! Form::label('Aktyvus nuo', '', ['class'=>'control-label']) !!}
{!! Form::text('active_from', @$active_from, ['class'=>'form-control datepicker']) !!}<br />

{!! Form::label('Aktyvus iki', '', ['class'=>'control-label']) !!}
{!! Form::text('active_to', @$active_to, ['class'=>'form-control datepicker']) !!}<br />

{!! Form::submit(trans('admin.submit'), ['class'=>'btn btn-info btn-lg']) !!}
