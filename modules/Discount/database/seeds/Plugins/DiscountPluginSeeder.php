<?php

use Illuminate\Database\Seeder;

class DiscountPluginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plugins')->insert([
            'name' => 'Discount',
            'slug' => 'Discount'
        ]);
    }
}
