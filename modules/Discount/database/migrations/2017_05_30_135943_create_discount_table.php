<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discounts', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('code')->nullable();
            $table->tinyInteger('type')->default(0);
            $table->decimal('size', 10, 2)->nullable();
            $table->tinyInteger('active')->default(0);
            $table->string('active_from')->nullable();
            $table->string('active_to')->nullable();
            $table->timestamps();
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->string('discount_name')->nullable();
            $table->decimal('discount', 10, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('discounts');
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('discount_name');
            $table->dropColumn('discount');
        });
    }
}
