<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');

            $table->smallInteger('status')->default(0)->nullable();
            $table->string('customer_id')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('total')->nullable();

            $table->integer('delivery_id')->nullable();
            $table->string('delivery_name')->nullable();
            $table->decimal('delivery_price', 10, 2)->default(0)->nullable();
            $table->integer('payment_id')->nullable();
            $table->string('payment_name')->nullable();

            $table->timestamps();
        });

        Schema::create('orders_products', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('order_id')->nullable();
            $table->integer('product_id')->nullable();

            $table->string('name')->nullable();
            $table->decimal('price', 10, 2)->default(0)->nullable();
            $table->integer('qty')->nullable();
            $table->string('combination_id')->nullable();
            $table->string('combination_name')->nullable();

            $table->timestamps();
            $table->index(['order_id', 'product_id']);
        });

        Schema::create('orders_settings', function (Blueprint $table) {
            $table->increments('id');

            $table->smallInteger('type')->nullable();
            $table->string('name')->nullable();
            $table->decimal('price', 10, 2)->default(0)->nullable();
            $table->text('body')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
        Schema::drop('orders_products');
        Schema::drop('orders_settings');
    }
}
