<?php

use Illuminate\Database\Seeder;

class OrderPluginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plugins')->insert([
            'name' => 'Užsakymai',
            'slug' => 'Order',
            'category_id' => 13
        ]);
        DB::table('categories')->insert([
            'id' => 13,
            'name' => 'Užsakymas',
            'slug' => 'uzsakymas'
        ]);

        DB::table('orders_settings')->insert([
            'id' => 1,
            'type' => 0,
            'name' => 'Pardavėjo rekvizitai',
            'body' => '<p><b>Beenet.lt</b></p><p>El. paštas: info@beenet.lt</p>'
        ]);
        DB::table('orders_settings')->insert([
            'id' => 2,
            'type' => 0,
            'name' => 'Sąskaitos serija',
            'body' => 'BNT'
        ]);
        DB::table('orders_settings')->insert([
            'id' => 3,
            'type' => 2,
            'name' => 'Sąskaita'
        ]);
        DB::table('orders_settings')->insert([
            'id' => 4,
            'type' => 2,
            'name' => 'Paysera'
        ]);
        DB::table('orders_settings')->insert([
            'id' => 5,
            'type' => 1,
            'name' => 'Pristatymas',
            'price' => 2
        ]);

        DB::table('orders_settings')->insert([
            'id' => 9,
            'type' => 0,
            'name' => 'Laiškas užsisakius',
            'body' => 'Sveiki, Jūsų užsakymas gautas.'
        ]);
        DB::table('orders_settings')->insert([
            'id' => 10,
            'type' => 0,
            'name' => 'Laiškas apmokėjus',
            'body' => 'Sveiki, Jūsų užsakymo apmokėjimas gautas.'
        ]);
        DB::table('orders_settings')->insert([
            'id' => 11,
            'type' => 0,
            'name' => 'Laiškas išsiuntus',
            'body' => 'Sveiki, Jūsų užsakymas išsiųstasd'
        ]);
        DB::table('orders_settings')->insert([
            'id' => 12,
            'type' => 0,
            'name' => 'Laiško footer',
            'body' => 'SILE'
        ]);
        DB::table('orders_settings')->insert([
            'id' => 13,
            'type' => 0,
            'name' => 'Laiškas užsisakius. Tema',
            'body' => 'BNT'
        ]);
        DB::table('orders_settings')->insert([
            'id' => 14,
            'type' => 0,
            'name' => 'Laiškas apmokėjus. Tema',
            'body' => 'Jūsų užsakymo apmokėjimas gautas'
        ]);
        DB::table('orders_settings')->insert([
            'id' => 15,
            'type' => 0,
            'name' => 'Laiškas išsiuntus. Tema',
            'body' => 'Jūsų užsakymas išsiųstas'
        ]);
    }
}
