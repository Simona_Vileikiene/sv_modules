<?php

namespace App\Http\Controllers\Frontend;

use App\Notifications\PurchaseNew;
use App\OrdersSetting;
use Artme\Paysera\Facades\Paysera;
use \Cart;
use Illuminate\Http\Request;

use App\Category;
use App\Order;
use App\OrdersProduct;
use App\Product;
use App\ProductsCombination;

use App\Http\Requests\OrderRequest;

class OrderController extends \App\Http\Controllers\Controller
{

    //order view
    protected function index(Request $request)
    {
        $page = Category::getCurrent();
        $cartDeliveryId = $request->session()->get('cart_delivery_id');
        return view('site/order/index', compact('page', 'cartDeliveryId'));
    }

    //save order
    public function orderStore($productId = null, $combinationId = null, $qty = 0, OrderRequest $request = null)
    {
        $item = Product::find($productId);
        if ($item) {
            $combination = ProductsCombination::find($combinationId);
            if ($combination->product_id != $item->id) {
                return redirect()->back()->with('orderError', 'Order error');
            }
            if (!$combination) {
                $combination = $item->combinations()->first();
            }
            $order = Order::create($request->all());
            $product = OrdersProduct::addOrderProductToOrder(
                $item,
                $combination->priceTrue,
                $qty,
                $combination->id,
                $combination->name
            );
            $order->products()->save($product);

            Order::finishOrder($request, $order);
            return redirect()->back()->with('orderMessage', 'Order OK');
        }
        return redirect()->back()->with('orderError', 'Order error');
    }

    //save order from cart
    public function orderCartStore(OrderRequest $request)
    {
        $order = Order::create($request->all());
        foreach (Cart::content() as $item) {
            $product = OrdersProduct::addOrderProductToOrder(
                $item,
                $item->price,
                $item->qty,
                $item->options->combination['id'],
                $item->options->combination['name']
            );
            $order->products()->save($product);
        }
        Order::finishOrder($request, $order);
        Cart::destroy();
        return redirect()->back()->with('orderMessage', 'Order OK');
    }

    public function orderSuccess()
    {
        Cart::destroy();
        return redirect(action('Frontend\OrderController@index'))->with('orderMessage', 'Order OK');
    }

    public function orderCancel()
    {
        return redirect(action('Frontend\OrderController@index'))->with('orderError', 'Order error');
    }

    //paysera form
    public function payseraCallback(Request $request)
    {
        $data = Paysera::parsePayment($request);
        if(isset($data['orderid'])) {
            Order::changeStatus($data['orderid'], 2);
            echo "OK";
        }
    }
}