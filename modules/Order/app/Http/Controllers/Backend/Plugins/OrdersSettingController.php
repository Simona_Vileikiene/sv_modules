<?php
namespace App\Http\Controllers;

use App\OrdersSetting;

use App\Http\Requests\OrdersSettingRequest;

class OrdersSettingController extends Controller
{
    //admin. list
    public function index()
    {
        $items = OrdersSetting::where('type', 1)->orderby('name')->get();
        $items2 = OrdersSetting::where('type', 2)->orderby('name')->get();
        $items3 = OrdersSetting::where('type', 0)->orderby('name')->get();
        return view('admin/ordersSetting/index', compact("items", 'items2', 'items3'));
    }

    //admin. create new
    public function create($type = 0)
    {
        return view('admin/ordersSetting/create', compact('type'));
    }

    //admin. save create
    public function store(OrdersSettingRequest $request)
    {
        $item = OrdersSetting::create($request->all());
        return redirect()->action('OrdersSettingController@index');
    }

    //admin. edit
    public function edit($id)
    {
        $item = OrdersSetting::findorFail($id);
        $type = $item->type;
        return view('admin/ordersSetting/edit', compact('item', 'type'));
    }

    //admin. save edit
    public function update($id, OrdersSettingRequest $request)
    {
        $item = OrdersSetting::findorFail($id);
        $item->update($request->all());
        return redirect()->action('OrdersSettingController@index');
    }

    //admin. destroy
    public function destroy($id)
    {
        $item = OrdersSetting::findorFail($id);
        $item->delete();
        return redirect()->action('OrdersSettingController@index');
    }

    //no used, but laravel should has it for resource route
    public function show()
    {
        exit();
    }

}
