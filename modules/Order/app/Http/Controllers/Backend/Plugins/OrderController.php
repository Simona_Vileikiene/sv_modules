<?php
namespace App\Http\Controllers;

use Request;
use DB;

use App\OrdersProduct;
use App\Order;

use App\Http\Requests\OrderRequest;

class OrderController extends Controller
{
    //admin. list
    public function index()
    {
        $items = Order::whereNotNull('id');
        if (!empty(Request::input('search'))) {
            $items = $items->where(function ($query) {
                $query->where('name', 'like', '%'.Request::input('name').'%')
                    ->orwhere('email', 'like', '%'.Request::input('email').'%');
            });
        }
        $items = $items->orderby("created_at", 'DESC')->paginate(50);
        return view('admin/order/index', compact("items"));
    }

    //admin. create new
    public function create($id = 0)
    {
        return view('admin/order/create', compact('id'));
    }

    //admin. save create
    public function store(OrderRequest $request)
    {
        $item = Order::create($request->all());
        OrdersProduct::manageOrderProducts($item, $request);
        Order::manageTotal($item);
        return redirect()->action('OrderController@index');
    }

    //admin. edit
    public function edit($id)
    {
        $item = Order::findorFail($id);
        return view('admin/order/edit', compact('item'));
    }

    //admin. save edit
    public function update($id, OrderRequest $request)
    {
        $item = Order::findorFail($id);
        $item->update($request->all());
        OrdersProduct::manageOrderProducts($item, $request);
        Order::manageTotal($item);
        return redirect()->action('OrderController@index');
    }

    //admin. destroy
    public function destroy($id)
    {
        $item = Order::findorFail($id);
        OrdersProduct::manageOrderProducts($item, null, true);
        $item->delete();
        return redirect()->action('OrderController@index');
    }

    //no used, but laravel should has it for resource route
    public function show()
    {
        exit();
    }

    //no used, but laravel should has it for resource route
    public function invoice($id)
    {
        $item = Order::findorFail($id);
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML(view('site/order/invoice', compact('item')));
        return $pdf->stream();
    }
}
