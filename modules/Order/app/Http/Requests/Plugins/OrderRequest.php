<?php

namespace App\Http\Requests;

use App\OrdersSetting;
use Illuminate\Foundation\Http\FormRequest;

use \Cart;
use Illuminate\Http\Request;


class OrderRequest extends FormRequest
{
        /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
        ];
        if(OrdersSetting::where('type', 1)->count()>0 && Cart::total()>0) {
            $rules['delivery_id'] = 'required';
        }
        if(OrdersSetting::where('type', 2)->count()>0 && Cart::total()>0) {
            $rules['payment_id'] = 'required';
        }
        $rules = array_merge($rules, $this->orderProductsValidation());
        $rules = array_merge($rules, $this->orderProductsValidation('products_new'));
        return $rules;
    }

    public function attributes()
    {
        $attributes = [
            'delivery_id' => 'Pristatymas',
            'payment_id' => 'Apmokėjimas'
        ];
        $attributes = array_merge($attributes, $this->orderProductsValidation(
            'products', true
        ));
        $attributes = array_merge($attributes, $this->orderProductsValidation(
            'products_new', true
        ));
        return $attributes;
    }

    protected function orderProductsValidation($fieldName = 'products', $isAttributes = false) {
        $items = [];
        if(Request::input($fieldName)) {
            foreach (Request::input($fieldName) as $k => $one) {
                if (!empty($one['name']) || !empty($one['price']) || !empty($one['qty'])) {
                    $combinationName = '#' . $k . ' prekės ';
                    if ($isAttributes) {
                        $items[$fieldName . '.' . $k . '.name'] = $combinationName . 'Pavadinimas';
                        $items[$fieldName . '.' . $k . '.price'] = $combinationName . 'Kaina';
                        $items[$fieldName . '.' . $k . '.qty'] = $combinationName . 'Kiekis';
                    } else {
                        $items[$fieldName . '.' . $k . '.name'] = 'required';
                        $items[$fieldName . '.' . $k . '.price'] = 'required|numeric';
                        $items[$fieldName . '.' . $k . '.qty'] = 'required|numeric';
                    }
                }
            }
        }
        return $items;
    }
}