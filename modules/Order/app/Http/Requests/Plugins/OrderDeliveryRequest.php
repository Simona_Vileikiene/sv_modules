<?php

namespace App\Http\Requests;

use App\OrdersSetting;
use Illuminate\Foundation\Http\FormRequest;

class OrderDeliveryRequest extends FormRequest
{
        /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        if(OrdersSetting::where('type', 1)->count()>0) {
            $rules['delivery_id'] = 'required';
        }
        return $rules;
    }

    public function attributes()
    {
        $attributes = [
            'delivery_id' => 'Pristatymas'
        ];
        return $attributes;
    }
}