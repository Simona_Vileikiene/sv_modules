<?php

namespace App;

use App;
use Request;
use Illuminate\Database\Eloquent\Model;

class OrdersProduct extends Model
{
    //no guarded, cause multi lang has unnecessary fields
    protected $fillable = ['order_id', 'product_id', 'name', 'price', 'qty', 'combination_id', 'combination_name'];

    //RALATIONSHIPS. each order product has one order
    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    //RALATIONSHIPS. each order product has one product
    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    //add product from site to order
    static function addOrderProductToOrder($item, $price, $qty, $combination_id, $combination_name){
        return new OrdersProduct([
            'product_id' => $item->id,
            'name' => $item->name,
            'price' => $price,
            'qty' => $qty,
            'combination_id' => $combination_id,
            'combination_name' => $combination_name
        ]);
    }

    //manage product combinations
    static function manageOrderProducts($item, $request = NULL, $deleteAll = false)
    {
        //delete all product combinations
        if ($deleteAll) {
            $item->products()->delete();
        }
        if (!empty($request) && $request->input('products')) {
            foreach ($request->input('products') as $one) {
                $combination = OrdersProduct::find($one['id']);
                if (isset($one['delete']) && $one['delete'] == 1) {
                    $combination->delete();
                } else {
                    $combination->update([
                        'name' => $one['name'],
                        'price' => $one['price'],
                        'qty' => $one['qty'],
                        'combination_name' => $one['combination_name']
                    ]);
                }
            }
        }
        //add new
        if (!empty($request) && $request->input('products_new')) {
            foreach ($request->input('products_new') as $one) {
                if (!empty($one['price']) || !empty($one['qty']) || !empty($one['name'])) {
                    $combination = new OrdersProduct([
                        'name' => $one['name'],
                        'price' => $one['price'],
                        'qty' => $one['qty'],
                        'combination_name' => $one['combination_name']
                    ]);
                    $item->products()->save($combination);
                }
            }
        }
    }
}