<?php

namespace App;

use App\Notifications\Purchase;
use Artme\Paysera\Facades\Paysera;
use Illuminate\Notifications\Notifiable;

use DB;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use Notifiable;

    //no guarded, cause multi lang has unnecessary fields
    protected $fillable = ['customer_id', 'name', 'email', 'phone', 'total',
        'delivery_id', 'delivery_name', 'delivery_price', 'payment_id', 'payment_name', 'status'];

    CONST STATUSES = ['Atšauktas', 'Laukia apmokėjimo', 'Apmokėtas', 'Išsiųstas'];

    //RALATIONSHIPS. one order have multi products
    public function products()
    {
        return $this->hasMany('App\OrdersProduct');
    }

    //get InvoiceName attribute [$item->InvoiceName]
    protected function getInvoiceNameAttribute()
    {
       return strip_tags(@OrdersSetting::find(2)->body.' '.$this->id);
    }

    //get img attribute [$item->InvoiceNameFull]
    protected function getInvoiceNameFullAttribute()
    {
        return 'Sąskaita '.$this->invoiceName;
    }

    //manage order settings: delivery & payment
    static function manageSettings($request, $item) {
        $data = [];
        $delivery = @OrdersSetting::find($request->input('delivery_id'));
        if($delivery) {
            $data['delivery_id'] = $delivery->id;
            $data['delivery_name'] = $delivery->name;
            $data['delivery_price'] = $delivery->price;
        }
        $payment = @OrdersSetting::find($request->input('payment_id'));
        if($payment) {
            $data['payment_id'] = $payment->id;
            $data['payment_name'] = $payment->name;
        }
        $item->update($data);
    }

    //manage order total
    static function manageTotal($item) {
        $total = round($item->products()->sum(DB::raw('qty*price')) + $item->delivery_price, 2);
        $item->update(['total' => $total]);
    }

    //finish order
    static function finishOrder($request, $order) {
        Order::manageSettings($request, $order);
        Order::manageTotal($order);
        //paysera - go to paysera
        if($order->payment_id == 4) {
            $options['accepturl'] = action('Frontend\OrderController@orderSuccess');
            $options['cancelurl'] = action('Frontend\OrderController@orderCancel');
            $options['p_email'] = $order->email;
            return Paysera::makePayment($order->id, $order->total, $options);
        }
        $status = 1;
        if($order->total == 0) {
            $status = 2;
        }
        Order::changeStatus($order->id, $status);
    }

    //change status
    static function changeStatus($id, $newStatus = 0) {
        $item = Order::findOrFail($id);
        if($item->status != $newStatus) {
            if($newStatus == 1) {
                Order::notifyPurchaseNew($item);
            } elseif($newStatus == 2) {
                Order::notifyPurchasePaid($item);
            } elseif($newStatus == 3) {
                Order::notifyPurchaseSend($item);
            }
            $item->status = $newStatus;
            $item->save();
        }
    }

    //notify new purchase
    static function notifyPurchaseNew($order) {
        $subject = strip_tags(OrdersSetting::find(13)->body);
        $body = OrdersSetting::find(9)->body;
        $order->notify(new Purchase($order, $subject, $body));
    }

    //notify purchase paid
    static function notifyPurchasePaid($order) {
        $subject = strip_tags(OrdersSetting::find(14)->body);
        $body = OrdersSetting::find(10)->body;
        $order->notify(new Purchase($order, $subject, $body));
    }

    //notify purchase send
    static function notifyPurchaseSend($order) {
        $subject = strip_tags(OrdersSetting::find(15)->body);
        $body = OrdersSetting::find(11)->body;
        $order->notify(new Purchase($order, $subject, $body));
    }

    //invoice total in words
    static function totalInWords($total) {
        list($lt, $ct) = explode('.', $total);
        $sk1=array('','Vienas','Du','Trys','Keturi','Penki','Šeši','Septyni','Aštuoni','Devyni');
        $sk2=array('Dešimts','Vienuolika','Dvylika','Trylika','Keturiolika','Penkiolika','Šešiolika','Septiniolika','Aštuoniolika','Devyniolika');
        $sk3=array('Dvidešimt','Trisdešimt','Keturiasdešimt','Penkiasdešimt','Šešiasdešimt','Septyniasdešimt','Aštuoniasdešimt','Devyniasdešimt');
        $sk4=array('Šimtas','Tūkstantis');
        if ($lt<10) {
            $zodziai=$sk1[$lt];
        } elseif ($lt<20) {
            $zodziai=$sk2[$lt-10];
        } elseif ($lt<100) {
            $de=floor($lt/10);
            $vn=$lt%10;
            $zodziai=$sk3[$de-2]." ".$sk1[$vn];
        } elseif ($lt<1000) {
            $sm=floor($lt/100);
            if ($sm==0) {
                $zodziai="";
            } elseif ($sm==1) {
                $zodziai="Šimtas ";
            } else {
                $zodziai=$sk1[$sm]." Šimtai ";
            }
            $des=$lt%100;
            if ($des<10) {
                $zodziai.=$sk1[$des];
            } elseif ($des<20) {
                $zodziai.=$sk2[$des-10];
            } elseif ($des<100) {
                $de=floor($des/10);
                $vn=$des%10;
                $zodziai.=$sk3[$de-2]." ".$sk1[$vn];
            }
        } elseif ($lt<10000) {
            $tk=floor($lt/1000);
            if ($tk==0) {
                $zodziai="";
            } elseif ($tk==1) {
                $zodziai="Tūkstantis ";
            } else {
                $zodziai=$sk1[$tk]." Tūkstančiai ";
            }
            $sm=$lt%1000; $sm=floor($sm/100);
            if ($sm==0) {
                $zodziai.="";
            } elseif ($sm==1) {
                $zodziai.="Šimtas ";
            } else {
                $zodziai.=$sk1[$sm]." Šimtai ";
            }
            $des=$lt%100;
            if ($des<10) {
                $zodziai.=$sk1[$des];
            } elseif ($des<20) {
                $zodziai.=$sk2[$des-10];
            } elseif ($des<100) {
                $de=floor($des/10);
                $vn=$des%10;
                $zodziai.=$sk3[$de-2]." ".$sk1[$vn];
            }
        } else {
            $bt=floor($lt/1000);
            $zodziai=$bt." Tūkstančių ";
            $tk=$lt%10000; $tk=floor($tk/1000);
            $sm=$lt%1000; $sm=floor($sm/100);
            if ($sm==0) {
                $zodziai.="";
            } elseif ($sm==1) {
                $zodziai.="Šimtas ";
            } else {
                $zodziai.=$sk1[$sm]." Šimtai ";
            }
            $des=$lt%100;
            if ($des<10) {
                $zodziai.=$sk1[$des];
            } elseif ($des<20) {
                $zodziai.=$sk2[$des-10];
            } elseif ($des<100) {
                $de=floor($des/10);
                $vn=$des%10;
                $zodziai.=$sk3[$de-2]." ".$sk1[$vn];
            }
        }
        return $zodziai.' eurai, '.$ct.' ct';
    }
}