<?php

namespace App;

use App;
use Request;
use Illuminate\Database\Eloquent\Model;

class OrdersSetting extends Model
{
    //no guarded, cause multi lang has unnecessary fields
    protected $fillable = ['type', 'name', 'price', 'body'];
}