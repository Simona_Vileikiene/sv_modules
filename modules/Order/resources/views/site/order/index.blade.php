@extends('resources.views.site.layout')

@section('content')

    <article id="product-view" class="width col-sm-5">
        <h1>{{ $page->name }}</h1>

        Užsakymas
        @if (Session::get('orderMessage'))
            <p class="alert alert-success">{{ Session::get('orderMessage') }}</p>
        @elseif (Session::get('orderError'))
            <p class="alert alert-danger">{{ Session::get('orderError') }}</p>
        @elseif(Cart::count() == 0)
            <p class="alert alert-danger">{{ Session::get('orderError') }}Prekių krepšelis tuščias</p>
        @else
            @include('errors/list')
            {!! Form::open(array('action' => ['Frontend\OrderController@orderCartStore'], 'files' => true)) !!}
            @include('site/order/form')

            @if(App\OrdersSetting::where('type', 1)->count()>0  && Cart::total()>0)
                @if($cartDeliveryId>0)
                    {!! Form::hidden('delivery_id', $cartDeliveryId) !!}
                @else
                    {!! Form::label('Pristatymas', '', ['class'=>'control-label']) !!}<br/>
                    @foreach(App\OrdersSetting::where('type', 1)->get() as $item)
                        {!! Form::radio('delivery_id', $item->id, @$delivery_id, ['class'=>'radio', 'id' => 'delivery_id-'.$item->id]) !!}
                        {!! Form::label('delivery_id-'.$item->id, $item->name.' ('.$item->price.' Eur') !!}<br/>
                    @endforeach
                    <br/>
                @endif
            @endif

            @if(App\OrdersSetting::where('type', 2)->count()>0  && Cart::total()>0)
                {!! Form::label('Apmokėjimas', '', ['class'=>'control-label']) !!}<br/>
                @foreach(App\OrdersSetting::where('type', 2)->get() as $item)
                    {!! Form::radio('payment_id', $item->id, @$payment_id, ['class'=>'radio', 'id' => 'payment_id-'.$item->id]) !!}
                    {!! Form::label('payment_id-'.$item->id, $item->name) !!}<br/>
                @endforeach
                <br/>
            @endif

            {!! Form::submit('Siųsti', ['class'=>'btn btn-info btn-lg']) !!}
            {!! Form::close() !!}
        @endif

    </article>


@endsection
