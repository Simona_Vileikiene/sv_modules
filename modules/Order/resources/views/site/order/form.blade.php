{!! Form::label('Vardas*', '', ['class'=>'control-label']) !!}
{!! Form::text('name', @$name, ['class'=>'form-control']) !!}<br/>

{!! Form::label('El. paštas*', '', ['class'=>'control-label']) !!}
{!! Form::text('email', @$email, ['class'=>'form-control']) !!}<br/>

{!! Form::label('Telefonas', '', ['class'=>'control-label']) !!}
{!! Form::text('phone', @$phone, ['class'=>'form-control']) !!}<br/>