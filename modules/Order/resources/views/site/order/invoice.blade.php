<html>
<head>
    <title>{{ $item->InvoiceNameFull }}</title>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
    <style>
        body {
            font-family: DejaVu Sans;
            line-height: 1.2;
            font-size: 12px;
            color: #000;
        }

        h1 {
            text-align: center;
            margin: 0;
        }

        h2 {
            text-align: center;
            font-size: 14px;
            margin: 0;
        }

        h3 {
            text-align: center;
            font-size: 12px;
            font-weight: normal;
        }

        .part {
            width: 50%;
            display: inline-block;
            vertical-align: top;
        }

        td, th {
            padding: 5px;
            border: 1px solid #080808;
            text-align: center;
        }
         p {
             margin: 0;
         }
    </style>
</head>
<body>
<h1>SĄSKAITA</h1>
<h2>Nr. {{ $item->InvoiceName }}</h2>
<h3>{{ date('Y-m-d') }}</h3><br/>
<div class="part">
    {!! @\App\OrdersSetting::find(1)->body !!}
    Mokėjimo paskirtis: {{ $item->InvoiceName }}
</div>
<div class="part">
    <p><b>Pirkėjas: {{ $item->name }}</b></p>
    <p>El. paštas: {{ $item->email }}</p>
    @if(!empty($item->phone)) <p>Telefonas: {{ $item->phone }}</p> @endif
</div>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <th width="10%">Nr.</th>
        <th style="text-align: left">Pavadinimas</th>
        <th width="15%">Kaina</th>
        <th width="15%">Kiekis</th>
        <th width="15%" style="text-align: right">Suma</th>
    </tr>
    @foreach($item->products as $k=>$one)
        <tr>
            <td>{{ $k+1 }}</td>
            <td style="text-align: left">{{ $one->name.' '.$one->combination_name  }}</td>
            <td>{{ $one->price  }} Eur</td>
            <td>{{ $one->qty  }}</td>
            <td style="text-align: right">{{ number_format($one->price*$one->qty, 2, '.', '')  }} Eur</td>
        </tr>
    @endforeach
    @if(!empty($item->delivery_name))
        <tr>
            <td>{{ $k+1 }}</td>
            <td style="text-align: left">{{ $item->delivery_name  }}</td>
            <td>{{ $item->delivery_price  }} Eur</td>
            <td>1</td>
            <td style="text-align: right">{{ $item->delivery_price }} Eur</td>
        </tr>
    @endif
</table>
<br/><br/>
<table width="45%" cellpadding="0" cellspacing="0" align="right" style="font-weight: bold;">
    <tr>
        <td style="text-align: left">Viso mokėti:</td>
        <td style="text-align: right" width="33%">{{ number_format($item->total, 2, '.', '')  }} Eur</td>
    </tr>
</table>
<br/><br/><br/>
<div style="text-align: right;width:100%;clear: both">
    Suma žodžiais:
    {{ App\Order::totalInWords($item->total) }}
</div>


</body>
</html>