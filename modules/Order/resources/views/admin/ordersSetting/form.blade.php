@include('admin/langs')

{!! Form::hidden('type', $type, ['class'=>'form-control']) !!}

{!! Form::label('Pavadinimas*', '', ['class'=>'control-label']) !!}
{!! Form::text('name', @$name, ['class'=>'form-control']) !!}<br/>

{!! Form::label('Kaina', '', ['class'=>'control-label']) !!}
{!! Form::text('price', @$price, ['class'=>'form-control']) !!}<br/>

{!! Form::label('Tekstas', '', ['class'=>'control-label']) !!}
<span class="multi" name="body">{!! Form::textarea('body', @$body, ['class'=>'form-control redactor']) !!}</span>
<br/>

{!! Form::submit('Siųsti', ['class'=>'btn btn-info btn-lg']) !!}
