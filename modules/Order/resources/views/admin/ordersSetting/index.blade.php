@extends('admin/layout')

@section('content')
    @if(Auth::guard('admin')->user()->id==1)
        <div class="col-md-12 text-right" style="padding-top:25px;">
            <a href="{{ action("OrdersSettingController@create", 0) }}" class="btn btn-success pull-right">
                <i class="glyphicon glyphicon-plus"></i> Sukurti naują
            </a>
        </div>
    @endif
    <div class="clearfix"></div><br/>

    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>#</th>
                <th>Pavadinimas</th>
                <th></th>
                <th width="100"></th>
            </tr>
            @foreach($items3 as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{!! $item->body !!}</td>
                    <td>
                        <a href="{{ action("OrdersSettingController@edit", $item->id) }}" class="btn btn-info"
                           title="Redaguoti"><i class="glyphicon glyphicon-pencil"></i></a>
                        @if(Auth::guard('admin')->user()->id==1)
                            {!! Form::model($item, ['method' => 'DELETE', 'action' => ['OrdersSettingController@destroy', $item->id], 'class'=>'del-form', 'onclick'=>"return confirm('Ar tikrai norite ištrinti?')", 'title'=>'Ištrinti']) !!}
                            {!! Form::submit(' X ', ['class'=>'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        @endif
                    </td>
                </tr>

            @endforeach
        </table>

        <h1 class="col-md-2">Pristatymai</h1>
        <div class="col-md-10 text-right" style="padding-top:25px;">
            <a href="{{ action("OrdersSettingController@create", 1) }}" class="btn btn-success pull-right">
                <i class="glyphicon glyphicon-plus"></i> Sukurti naują
            </a>
        </div>
        <div class="clearfix"></div>
        <br/>
        <table class="table table-hover">
            <tr>
                <th>#</th>
                <th>Pavadinimas</th>
                <th>Kaina</th>
                <th width="100"></th>
            </tr>
            @foreach($items as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->price }}</td>
                    <td>
                        <a href="{{ action("OrdersSettingController@edit", $item->id) }}" class="btn btn-info"
                           title="Redaguoti"><i class="glyphicon glyphicon-pencil"></i></a>
                        {!! Form::model($item, ['method' => 'DELETE', 'action' => ['OrdersSettingController@destroy', $item->id], 'class'=>'del-form', 'onclick'=>"return confirm('Ar tikrai norite ištrinti?')", 'title'=>'Ištrinti']) !!}
                        {!! Form::submit(' X ', ['class'=>'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>

            @endforeach
        </table>
    </div>

    <h1 class="col-md-2">Mokėjimai</h1>
    @if(Auth::guard('admin')->user()->id==1)
        <div class="col-md-10 text-right" style="padding-top:25px;">
            <a href="{{ action("OrdersSettingController@create", 2) }}" class="btn btn-success pull-right">
                <i class="glyphicon glyphicon-plus"></i> Sukurti naują
            </a>
        </div>
    @endif
    <div class="clearfix"></div><br/>

    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>#</th>
                <th>Pavadinimas</th>
                <th width="100"></th>
            </tr>
            @foreach($items2 as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>
                        <a href="{{ action("OrdersSettingController@edit", $item->id) }}" class="btn btn-info"
                           title="Redaguoti"><i class="glyphicon glyphicon-pencil"></i></a>
                        @if(Auth::guard('admin')->user()->id==1)
                            {!! Form::model($item, ['method' => 'DELETE', 'action' => ['OrdersSettingController@destroy', $item->id], 'class'=>'del-form', 'onclick'=>"return confirm('Ar tikrai norite ištrinti?')", 'title'=>'Ištrinti']) !!}
                            {!! Form::submit(' X ', ['class'=>'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        @endif
                    </td>
                </tr>

            @endforeach
        </table>
    </div>
@endsection 