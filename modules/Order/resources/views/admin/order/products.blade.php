<div class="combination">
    <b data-name="combination_no">#{{ $id  }}</b>

    {!! Form::label('Prekė*', '', ['class'=>'control-label']) !!}<br />
    <div class="col-md-6" style="padding-right:10px">
    {!! Form::text($fieldName.'['.$id.'][name]', ($product)?$product->name:old($fieldName.'['.$id.'][name]'),
    ['class'=>'form-control']) !!}</div>
    <div class="col-md-6" style="padding-right:10px">
        {!! Form::text($fieldName.'['.$id.'][combination_name]',
        ($product)?$product->combination_name:old($fieldName.'['.$id.'][combination_name]'),
        ['class'=>'form-control']) !!}</div>

    {!! Form::label('Kaina*', '', ['class'=>'control-label']) !!}
    {!! Form::text($fieldName.'['.$id.'][price]', ($product)?$product->price:old($fieldName.'['.$id.'][price]'),
    ['class'=>'form-control']) !!}<br/>

    <div class="col-md-6" style="padding-right:10px">
        {!! Form::label('Kiekis*', '', ['class'=>'control-label']) !!}
        {!! Form::text($fieldName.'['.$id.'][qty]', ($product)?$product->qty:old($fieldName.'['.$id.'][qty]'),
        ['class'=>'form-control']) !!}<br/>
    </div>
    <div class="col-md-6" style="padding-top:30px">
        {!! Form::hidden($fieldName.'['.$id.'][id]', $id, false, ['class'=>'form-control radio']) !!}
        @if($product)
            {!! Form::checkbox($fieldName.'['.$id.'][delete]', 1, false, ['class'=>'form-control radio']) !!}
            <small>Pašalinti</small>
        @endif
    </div>
    <div class="clearfix"></div>
</div>