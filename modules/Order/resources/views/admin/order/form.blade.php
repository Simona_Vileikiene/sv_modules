{!! Form::label('Statusas', '', ['class'=>'control-label']) !!}<br/>
{!! Form::select('status', \App\Order::statuses, @$statuse, ['class'=>'form-control']) !!}<br/>

@include('site/order/form')

{!! Form::hidden('delivery_id', (isset($item))?$item->delivery_id:0, ['class'=>'form-control']) !!}
{!! Form::label('Pristatymo pavadinimas', '', ['class'=>'control-label']) !!}<br/>
{!! Form::text('delivery_name', @$delivery_name, ['class'=>'form-control']) !!}<br/>
{!! Form::label('Pristatymo kaina', '', ['class'=>'control-label']) !!}
{!! Form::text('delivery_price', @$delivery_price, ['class'=>'form-control']) !!}<br/>

{!! Form::hidden('payment_id', (isset($item))?$item->payment_id:0, ['class'=>'form-control']) !!}
{!! Form::label('Apmokėjimo pavadinimas', '', ['class'=>'control-label']) !!}
{!! Form::text('payment_name', @$payment_name, ['class'=>'form-control']) !!}<br/>


<h3>Prekės</h3>
@if(isset($item) && count($item->products)>0)
    <?php $fieldName = 'products'; ?>
    @foreach($item->products as $product)
        <?php $id = $product->id ?>
        @include('admin/order/products')
    @endforeach
@endif
<div id="combinations">
    <?php $fieldName = 'products_new'; ?>
    @for($i=1; $i<6; $i++)
        <?php $id = $i; $product = NULL; ?>
        @include('admin/order/products')
    @endfor
</div>
<div class="col-md-12">
    <a href="#combination-new" class="btn btn-success pull-right" data-name="addNewCombination">
        <i class="glyphicon glyphicon-plus"></i></a>
</div>


{!! Form::submit('Siųsti', ['class'=>'btn btn-info btn-lg']) !!}
