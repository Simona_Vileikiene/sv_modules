@extends('admin.layout')

@section('content')

    <h1 class="col-md-2">Užsakymai</h1>
    {!! Form::open(array('method' => 'get', 'action' => 'OrderController@index', 'class'=>'text-center col-md-7', 'style'=>'padding-top:25px;')) !!}
    <div class="col-md-8">
        {!! Form::text('search', Request::input('search'), ['class'=>'form-control', 'placeholder'=>'Vardas, El. paštas, Prekė']) !!}
    </div>
    <div class="col-md-4 text-left">
        {!! Form::submit('Search', ['class'=>'btn btn-info']) !!}
        @if(Request::input('search')!='')
            <a href="{{ action("OrderController@index") }}" class="btn btn-default">Išvalyti</a>
        @endif
    </div>
    {!! Form::close() !!}
    <div class="col-md-3 text-right" style="padding-top:25px;">
        <a href="{{ action("OrdersSettingController@index") }}" class="btn btn-primary pull-right"
           style="margin-left:10px;">
            <i class="glyphicon glyphicon-cog"></i> Nustatymai
        </a>
        <a href="{{ action("OrderController@create") }}" class="btn btn-success pull-right">
            <i class="glyphicon glyphicon-plus"></i> Sukurti naują
        </a>
    </div>
    <div class="clearfix"></div><br/>


    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>#</th>
                <th>Statusas</th>
                <th>Vardas</th>
                <th>El. paštas</th>
                <th>Telefonas</th>
                <th>Suma</th>
                <th>Data</th>
                <th>Prekės</th>
                @if(App\OrdersSetting::where('type', 1)->count()>0)
                    <th>Pristatymas</th>
                @endif
                @if(App\OrdersSetting::where('type', 2)->count()>0)
                    <th>Apmokėjimas</th>
                @endif
                <th width="150"></th>
            </tr>
            @foreach($items as $item)

                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ @App\Order::statuses[$item->status] }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->email }}</td>
                    <td>{{ $item->phone }}</td>
                    <td>{{ $item->total }} Eur</td>
                    <td>{{ $item->created_at->format('Y-m-d') }}</td>
                    <td>
                        @if(count($item->products)>0)
                            <details>
                                <summary>Prekės</summary>
                                <table class="table">
                                    @foreach($item->products as $one)
                                        <tr>
                                            <td>{{ $one->name.' '.$one->combination_name  }}</td>
                                            <td>{{ $one->qty  }}</td>
                                            <td>{{ $one->price  }} Eur</td>
                                        </tr>
                                    @endforeach

                                </table>
                            </details>
                        @endif
                    </td>
                    @if(App\OrdersSetting::where('type', 1)->count()>0)
                        <td>{{ $item->delivery_name }} ({{ $item->delivery_price }})</td>
                    @endif
                    @if(App\OrdersSetting::where('type', 2)->count()>0)
                        <td>{{ $item->payment_name }}</td>
                    @endif
                    <td>
                        <a href="{{ action("OrderController@invoice", $item->id) }}" class="btn btn-warning"
                           title="Sąskaita" target="_blank"><i class="glyphicon glyphicon-list-alt"></i></a>
                        <a href="{{ action("OrderController@edit", $item->id) }}" class="btn btn-info"
                           title="Redaguoti"><i class="glyphicon glyphicon-pencil"></i></a>
                        {!! Form::model($item, ['method' => 'DELETE', 'action' => ['OrderController@destroy', $item->id], 'class'=>'del-form', 'onclick'=>"return confirm('Ar tikrai norite ištrinti?')", 'title'=>'Ištrinti']) !!}
                        {!! Form::submit(' X ', ['class'=>'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>

            @endforeach
        </table>
    </div>

    {!! $items->appends(['search' => Request::input('search')])->render() !!}
@endsection 