<?php

namespace App;

use Illuminate\Http\Response;
use Omniva\Client;
use Omniva\Parcel;
use Omniva\Address;
use Omniva\PickupPoint;
use Omniva\Service;

class OmninaHelper
{
    public function getPickupPoints($selected = NULL, $type = FALSE)
    {
        $client = new Client(env('OMNIVA_USERNAME'), env('OMNIVA_PASSWORD'));
        $pickupPoints = $client->getPickupPoints();
        if ($selected) {
            return $this->groupTerminals($pickupPoints, 'LT', $selected, $type);
        }
        return $this->groupTerminals($pickupPoints);
    }

    private function groupTerminals($terminals, $country = 'LT', $selected = '', $type = FALSE)
    {
        $parcel_terminals = ['' => 'Paštomatas'];
        if (is_array($terminals)) {
            foreach ($terminals as $terminal) {
                if ($selected == $terminal[0] && $type) {
                    return $terminal[2];
                } else if ($selected == $terminal[0]) {
                    return $terminal[1] . ', ' . $terminal[5];
                } elseif (isset($terminal[3]) && $terminal[3] == $country) {
                    $parcel_terminals[(string)$terminal[0]] = $terminal[1] . ', ' . $terminal[5];
                }
            }
        }
        return $parcel_terminals;
    }

    public function createOrder($order)
    {
        $client = new Client(env('OMNIVA_USERNAME'), env('OMNIVA_PASSWORD'));

        if (empty($order->tracking_number)) {
            $omnivaParcel = new Parcel();
            $omnivaParcel
                ->setWeight(0)
                ->setPartnerId($order->id);

            $sender = new Address();
            $sender
                ->setCountryCode('LT')
                ->setName(trans('db.pageTitle'))
                ->setEmail(trans('db.pageEmail'))
                ->setPhone(trans('db.pagePhone'))
                ->setCity(@OrdersSetting::find(17)->value)
                ->setStreet(@OrdersSetting::find(18)->value)
                ->setPostCode(@OrdersSetting::find(19)->value)
            ;

            $omnivaParcel->setSender($sender);

            $returnee = clone $sender;
            $omnivaParcel->setReturnee($returnee);

            $receiver = new Address();
            $receiver
                ->setCountryCode('LT')
                ->setName($order->name)
                ->setPhone('+37062147322')
                ->setEmail($order->email);

            if ($order->delivery_id == env('OMNIVA_DELIVER_ID')) {

                $pickupPoint = new PickupPoint($order->pickup_point);
                $pickupPoint->setType($this->getPickupPoints($order->pickup_point, TRUE) ? PickupPoint::TYPE_POST_OFFICE : PickupPoint::TYPE_TERMINAL);
                $receiver->setPickupPoint($pickupPoint);
                $omnivaParcel->addService(Service::SMS());
                $omnivaParcel->addService(Service::EMAIL());
            } else {
                $receiver
                    ->setCity($order->city)
                    ->setStreet($order->address)
                    ->setPostCode($order->postal_code);
            }
            $omnivaParcel->setReceiver($receiver);

            $response = $client->createShipment($omnivaParcel);
            if (!isset($response->savedPacketInfo)) {
                return $this->showError($response);
            }
            $trackingNumber = $response->savedPacketInfo->barcodeInfo->barcode;
            $order->update(['tracking_number' => $trackingNumber]);
        } else {
            $trackingNumber = $order->tracking_number;
        }

        $omnivaParcel->setTrackingNumber($trackingNumber);
        $response = $client->getLabel($omnivaParcel);
        if (isset($response->successAddressCards->addressCardData->fileData)) {
            $response = \Response::make($response->successAddressCards->addressCardData->fileData, 200);
            $response->header('Content-Type', 'application/pdf');
            return $response;
        }
        return $this->showError($response);
    }

    private function showError($response)
    {
        echo '<b>Klaida!</b> Padarykite šio lango nuotrauką ir nusiųskite programuotojui.';
        echo '<pre>';
        var_dump($response);
        exit;
    }

    public function manifest() {
        $order_table = '';
        foreach (Order::where('tracking_number', '!=', '')->orderby("created_at", 'DESC')->get() as $k => $order) {
            $i = $k + 1;
            $order_table .= '<tr><td width = "40" align="right">' . $i . '.</td><td>' . $order->tracking_number . '</td><td width = "60">' . date('Y-m-d') . '</td><td width = "40">1</td><td width = "60">0</td><td width = "210">' . $order->name . ', ' . $order->address . ', ' . $order->postal_code . ', ' . $order->city . ' Lietuva</td></tr>';
        }
        $tbl = '<html><head><title>Manifestas</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style>
        body {
            font-family: DejaVu Sans;
            line-height: 1.2;
            font-size: 12px;
            color: #000;
        }</style></head><body>
        <table cellspacing="0" cellpadding="4" border="1">
          <thead>
            <tr>
              <th width = "40" align="right">Nr.</th>
              <th>Siuntos numeris</th>
              <th width = "60">Data</th>
              <th width = "40" >Kiekis</th>
              <th width = "60">Svoris (kg)</th>
              <th width = "210">Gavėjo adresas</th>
            </tr>
          </thead>
          <tbody>
            ' . $order_table . '
          </tbody>
        </table><br/><br/>';
        $sign = 'Kurjerio vardas, pavardė, parašas ________________________________________________<br/><br/>';
        $sign .= 'Siuntėjo vardas, pavardė, parašas ________________________________________________</body></html>';
        return $tbl.$sign;
    }
}