<?php

use Illuminate\Database\Seeder;

class BlogPluginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'Blog kategorija',
            'slug' => 'blog',
            'priority' => DB::table('categories')->max('priority')+1
        ]);
        DB::table('plugins')->insert([
            'name' => 'Blog',
            'slug' => 'Blog',
            'settings' => json_encode(['category_id' => DB::table('categories')->max('id')])
        ]);

        //test item
        DB::table('blogs')->insert([
            'id' => 1,
            'name' => 'Testas blogas',
            'slug' => 'testas-blog',
            'body' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>',
            'category_id' => DB::table('categories')->max('id'),
            'data' => date('Y-m-d')
        ]);

        //multilang
        $multiLang = new App\MultiLang();
        if($multiLang->isMultiLang()) {
            //category
            DB::table('trans')->insert([
                'lang' => 'en',
                'item_module' => 'category',
                'item_id' => DB::table('categories')->max('id'),
                'item_field' => 'name',
                'body' => 'Blog category'
            ]);
            DB::table('trans')->insert([
                'lang' => 'en',
                'item_module' => 'category',
                'item_id' => DB::table('categories')->max('id'),
                'item_field' => 'slug',
                'body' => 'blog-category'
            ]);
            //blog. test item
            DB::table('trans')->insert([
                'lang' => 'en',
                'item_module' => 'blog',
                'item_id' => DB::table('blogs')->max('id'),
                'item_field' => 'name',
                'body' => 'Test blog'
            ]);
            DB::table('trans')->insert([
                'lang' => 'en',
                'item_module' => 'blog',
                'item_id' => DB::table('blogs')->max('id'),
                'item_field' => 'slug',
                'body' => 'test-blog'
            ]);
            DB::table('trans')->insert([
                'lang' => 'en',
                'item_module' => 'blog',
                'item_id' => DB::table('blogs')->max('id'),
                'item_field' => 'body',
                'body' => '<p>EN Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>'
            ]);
        }
    }
}
