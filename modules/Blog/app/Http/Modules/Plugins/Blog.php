<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;
use Illuminate\Support\Str;
use Request;
use App\Image;
use App;

class Blog extends Model
{
    //no guarded, cause multi lang has unnecessary fields
    protected $fillable = ['name', 'slug', 'body', 'category_id', 'data'];

    //images gallery path
    const IMAGES_PATH = '/images/blog/';

    //site category
    static public function siteCategoryId() {
        $item = Category::where('plugin', 'blog')->first();
        if(!$item) {
            throw new \Exception("Plugin don't have a category.");
        }
        return $item->id;
    }

    //get img html atrribute [$item->img]
    protected function getImgAttribute()
    {
        return Image::getImgHtml($this->photo, Blog::IMAGES_PATH, $this->name);
    }

    //get link attribute [$item->link]
    protected function getLinkAttribute()
    {
        $blogHelper = new BlogHelper();
        return $blogHelper->getUrlByLang($this);
    }

    //get Description [$item->description]
    protected function getDescriptionAttribute() {
        return Str::limit(strip_tags($this->body), 120);
    }

    /**
     * Prepare item translation
     *
     * @param TranHelper|null $tranHelper
     * @return Category
     */
    public function translate($tranHelper = null)
    {
        $tranHelper = ($tranHelper) ? $tranHelper : new TranHelper();
        return $tranHelper->getTranslatedItem($this, 'blog');
    }

}

class BlogHelper {

    /**
     * @var MultiLang|null
     */
    public $multiLang = null;

    /**
     * @var TranHelper|null
     */
    public $tranHelper = null;

    /**
     * CategoryHelper constructor.
     *
     * @param MultiLang|null $multiLang
     */
    public function __construct(MultiLang $multiLang = null)
    {
        $this->multiLang = ($multiLang) ? $multiLang : new MultiLang();
        $this->tranHelper = new TranHelper($this->multiLang);
        $this->categoryHelper = new CategoryHelper($this->multiLang);
    }

    /**
     * Get current blog item
     *
     * @return |null
     */
    public function getCurrent()
    {
        $lang = App::getLocale();
        //check max level url paths
        $i = Category::MAX_LEVEL+2;
        while ($i > 0) {
            $slug = Request::segment($i);
            if ($this->multiLang->isMultiLang() && !$this->multiLang->isDefaultLang()) {
                $transItems = Tran::where('lang', $lang)->where("item_module", "blog")
                    ->where("item_field", "slug")->where("body", $slug)->pluck('item_id')->toArray();
                $categoryByAutoSlug = explode($this->multiLang->currentLang.'-blog-', $slug);
                if(isset($categoryByAutoSlug[1])) {
                    $transItems[] = $categoryByAutoSlug[1];
                }
                $categories = Blog::wherein('id', $transItems)->get();
            } else {
                $categories = Blog::where('slug', $slug)->get();
            }
            foreach ($categories as $item) {
                $categoryId = Blog::siteCategoryId();
                if($item->category_id>0) {
                    $categoryId = $item->category_id;
                }
                $url = substr($this->categoryHelper->getUrlByLang($categoryId).'/'.$item->translate()->slug, 1);
                if (Request::is($url . '*')) {
                    return $item;
                }
            }
            $i--;
        }
        return NULL;
    }

    /**
     * Get blog url by lang
     *
     * @param $item
     *
     * @return string
     */
    public function getUrlByLang($item)
    {
        $categoryId = Blog::siteCategoryId();
        if ($item->category_id > 0) {
            $categoryId = $item->category_id;
        }
        return $this->categoryHelper->getUrlByLang($categoryId) . '/' . $item->translate($this->tranHelper)->slug;
    }
}
