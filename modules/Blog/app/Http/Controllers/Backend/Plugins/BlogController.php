<?php 

namespace App\Http\Controllers; 

use App\Category;
use App\CategoryHelper;
use App\Priority;
use App\TranHelper;
use Request;
use App\Blog;
use App\Tran;
use App\Image;
use Illuminate\Validation\Validator;
use App\Http\Requests\BlogRequest;
use Response;

use Illuminate\Support\Str;

class BlogController extends Controller 
{
    public function __construct()
    {
        $this->categoryHelper = new CategoryHelper();
        $this->tranHelper = new TranHelper();
    }

    /**
     * Admin. create new
     *
     * Admin. Blog list
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (Request::input('category')>0) {
            $items = Blog::where('category_id', Request::input('category'))->orderby("data", 'DESC')->paginate(50);
        } else {
            $items = Blog::orderby("data", 'DESC')->paginate(50);
        }
        $categories = $this->categoryHelper->getAllSubcategoriesList(Blog::siteCategoryId());
       	return view('admin/blog/index', compact("items", 'categories'));
    }

    /**
     * Admin. create new
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($id=0)
    {
        $categories = $this->categoryHelper->getAllSubcategoriesList(Blog::siteCategoryId());
        return view('admin/blog/create', compact('id', 'categories'));
    }

    /**
     * Admin. save new
     *
     * @param BlogRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(BlogRequest $request)
    {
        $item = Blog::create($request->all());
        $item->update(["slug" => Str::slug($request->input("slug"))]);
        $item->update(["data" => ($request->input("data"))?$request->input("data"):date('Y-m-d')]);
        //upload new image
        if($request->file('photo')) {
            $imageName = Image::uploadImage($item, $request->file('photo'), Blog::IMAGES_PATH);
            $item->photo = $imageName;
            $item->save();
        }
        //if multi lang save values
        $this->tranHelper->setItemTranslations($item->id, Request::segment(2), $request);
        return redirect()->action('BlogController@index'); 
    }

    /**
     * Admin. edit
     *
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $item = Blog::findorFail($id);
        $categories = $this->categoryHelper->getAllSubcategoriesList(Blog::siteCategoryId());
        return view('admin/blog/edit', compact('item', 'categories'));
    }

    /**
     * Admin. save edit
     *
     * @param $id
     * @param BlogRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, BlogRequest $request)
    {
        $item = Blog::findorFail($id);
        $item->update($request->all());
        $item->update(["slug" => Str::slug($request->input("slug"))]);
        //delete old and upload new image
        if($request->file('photo')) {
            Image::removeImage($item->photo, Blog::IMAGES_PATH);
            $imageName = Image::uploadImage($item, $request->file('photo'), Blog::IMAGES_PATH);
            $item->photo = $imageName;
            $item->save();
        }
        //if multi lang save values
        $this->tranHelper->setItemTranslations($item->id, Request::segment(2), $request);
        return redirect()->action('BlogController@index'); 
    }

    /**
     * Admin. destroy
     *
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $item = Blog::findorFail($id);
        Image::removeImage($item->photo, Blog::IMAGES_PATH);
        //if multi lang remove values
        $this->tranHelper->removeItemTranslations($item->id, Request::segment(2));
        $item->delete();
        return redirect()->action('BlogController@index'); 
    }

    //no used, but laravel should has it for resource route
    public function show()
    {
        exit();
    }

}
