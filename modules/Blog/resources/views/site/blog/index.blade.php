@extends('site.layout')

@section('content')

	<h2>{{ $page->name }}</h2>
	@foreach($items as $item)
		<?php $item = $item->translate() ?>
		<article class="navbar navbar-default col-md-3">
			<h3>{{ $item->name }}</h3>
			<figure class="thumbnail">{!! $item->img !!}</figure>
			<a href="{{ $item->link }}" class="btn btn-info">plačiau</a>
		</article>
	@endforeach
    <div class="clearfix"></div>
	
@endsection 