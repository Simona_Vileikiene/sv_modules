
{!! Form::label('Nuotrauka', '', ['class'=>'control-label']) !!}
@if(isset($item) and $item->img!="")
    <span style="width:100px;display:inline-block" class="thumb">{!! $item->img !!}</span>
@endif
{!! Form::file('photo', ['class'=>'form-control']) !!}<br />

{!! Form::label('Data', '', ['class'=>'control-label', 'data-comment'=>' (Nieko neįvedus - automatiškai šios dienos)']) !!}
{!! Form::date('data', @$data, ['class'=>'form-control datepicker']) !!}<br />

{!! Form::label('Pavadinimas*', '', ['class'=>'control-label']) !!}
{!! Form::text('name', @$name, ['class'=>'form-control multi']) !!}<br />

{!! Form::label('Nuoroda*', '', ['class'=>'control-label', 'data-comment'=>' (Nieko neįvedus - automatiškai pagal pavadinimą; unikalus)']) !!}
{!! Form::text('slug', @$slug, ['class'=>'form-control multi']) !!}<br/>

{!! Form::label('Tekstas', '', ['class'=>'control-label']) !!}
<span class="multi" name="body">{!! Form::textarea('body', @$body, ['class'=>'form-control redactor']) !!}</span><br />

@if(count($categories) > 1)
    {!! Form::label('Kategorija', '', ['class'=>'control-label']) !!}
    {!! Form::select('category_id', $categories, @$category_id, ['class'=>'form-control']) !!}<br />
@else
    {!! Form::hidden('category_id', @$category_id) !!}
@endif


{!! Form::submit('Siųsti', ['class'=>'btn btn-outline-main  btn-lg']) !!}
