@extends('admin.layout')

@section('content')

    <div class="module-form">
        <h1>{{ trans('admin.edit') }}</h1>
		@include('errors/list')
		{!! Form::model($item, ['method' => 'PATCH', 'action' => ['BlogController@update', $item->id], 'files' => true]) !!}
			@include('admin/blog/form')
        {!! Form::close() !!}
	</div>

@endsection
