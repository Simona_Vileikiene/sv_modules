@extends('admin/layout')

@section('content')

    <div class="row">
        <h1 class="col-lg-9 text-lg-left text-center">{{ __('Blog') }}</h1>
        <div class="col-lg-3 text-right p-2">
            <a href="{{ action("BlogController@create") }}" class="btn btn-outline-success">
                <i class="fas fa-plus"></i> {{ trans('admin.create_new') }}
            </a>
        </div>
    </div>
    {!! Form::open(array('method' => 'get', 'action' => 'BlogController@index', 'class'=>'row mb-4')) !!}
        <div class="col-lg-3">
            {!! Form::select('category', $categories, Request::input('category'), ['class'=>'form-control']) !!}
        </div>
        <div class="col-lg-2">
            {!! Form::submit('Paieška', ['class'=>'btn btn-outline-main']) !!}
            @if(Request::input('category')>0)
                <a href="{{ action("BlogController@index@index") }}" class="btn btn-outline-main">Išvalyti</a>
            @endif
        </div>
    {!! Form::close() !!}

    <div class="table-responsive">
        <table class="table table-hover  actions-2">
            <tr>
                <th>#</th>
                <th>Data</th>
                <th>Nuotrauka</th>
                <th>Pavadinimas</th>
                <th>Nuoroda</th>
                <th>Kategorija</th>
                <th width="200"></th>
            </tr>
            @foreach($items as $item)

                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->data }}</td>
                    <td class="thumb">{!! $item->img !!}</td>
                    <td>{{ $item->name }}</td>
                    <td><a href="{{ Request::root().$item->link }}"
                           target="_blank">{{ Request::root().$item->link }}</a></td>
                    <td>{{ App\Category::getNameById($item->category_id) }}</td>
                    <td>
                        <a href="{{ action("BlogController@edit", $item->id) }}" class="btn btn-sm btn-outline-info" title="Redaguoti"><i class="far fa-edit"></i></a>
                        {!! Form::model($item, ['method' => 'DELETE', 'action' => ['BlogController@destroy', $item->id], 'class'=>'d-inline', 'onclick'=>"return confirm('Ar tikrai norite ištrinti?')", 'title'=>'Ištrinti']) !!}
                        {!! Form::submit(' X ', ['class'=>'btn btn-sm btn-outline-danger']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>

            @endforeach
        </table>
    </div>

    {!! $items->appends(['category' => Request::input('category')])->render() !!}
@endsection
