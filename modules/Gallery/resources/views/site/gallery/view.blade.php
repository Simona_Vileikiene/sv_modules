@extends('resources.views.site.layout')

@section('content')

		
		<article >
			<h1>{{ $pageItem->name }}</h1>


			{!! $pageItem->body !!}
			<div class="row">
			@foreach($pageItem->images()->orderBy('priority')->get() as $k => $one)
				<figure class="col-md-2 col-sm-4 col-xs-12">
					{!! $one->imglink !!}</figure>

			@endforeach
			</div>
		</article>
        <div class="clearfix"></div> 
	
@endsection 