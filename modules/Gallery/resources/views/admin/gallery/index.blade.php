@extends('admin/layout')

@section('content')

    <h1 class="pull-left">Gallery</h1>
    <a href="{{ action("GalleryController@create") }}" class="btn btn-success pull-right">
        <i class="glyphicon glyphicon-plus"></i> Sukurti naują
    </a>
    <div class="clearfix"></div><br/>

    <div class="table-responsive">
        <table class="table table-hover " data-sortable="\App\Gallery">
            <tr @if(Request::input('sortable')) class="sortable-disable" @endif>
            <tr>
                <th>#</th>
                <th>Nuotrauka</th>
                <th>Pavadinimas</th>
                <th>Nuoroda</th>
                <th width="150">
                    @if(Request::input('sortable'))
                        <a href="{{ action("GalleryController@index") }}" class="btn btn-blank pull-right"
                           title="Stop eiliškumui" style="cursor: pointer">
                            <i class="glyphicon glyphicon-move"></i>
                        </a>
                    @else
                        <a href="{{ action("GalleryController@index") }}?sortable=1" class="btn btn-warning pull-right"
                           title="Kiesti eiliškumą">
                            <i class="glyphicon glyphicon-sort"></i>
                        </a>
                    @endif
                </th>
            </tr>
            @foreach($items as $item)

                <tr data-sortable-id="{{ $item->id }}">
                    <td>{{ $item->id }}</td>
                    <td class="thumb">{!! $item->img !!}</td>
                    <td>{{ $item->name }}</td>
                    <td><a href="{{ Request::root().$item->link }}"
                           target="_blank">{{ Request::root().$item->link }}</a></td>
                    <td>
                        <a href="{{ action("GalleryController@edit", $item->id) }}" class="btn btn-info"
                           title="Redaguoti"><i class="glyphicon glyphicon-pencil"></i></a>
                        {!! Form::model($item, ['method' => 'DELETE', 'action' => ['GalleryController@destroy', $item->id], 'class'=>'del-form', 'onclick'=>"return confirm('Ar tikrai norite ištrinti?')", 'title'=>'Ištrinti']) !!}
                        {!! Form::submit(' X ', ['class'=>'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>

            @endforeach
        </table>
    </div>

    {!! $items->appends(['search' => Request::input('search')])->render() !!}
@endsection 