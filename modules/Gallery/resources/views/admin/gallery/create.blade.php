@extends('admin/layout')

@section('content')

	<div class="col-md-6 col-md-offset-3">
		<h1>Sukurti naują</h1>
		@include('errors/list') 
		{!! Form::open(array('action' => ['GalleryController@store'], 'files' => true)) !!}
			@include('admin/gallery/form')
		{!! Form::close() !!}
	</div>
	
@endsection