@include('admin/langs')

<?php $fakeId = old('fakeId', time()); ?>
{!! Form::hidden('fakeId', $fakeId, ['class'=>'control-label']) !!}

{!! Form::label('Pavadinimas*', '', ['class'=>'control-label']) !!}
{!! Form::text('name', @$name, ['class'=>'form-control multi']) !!}<br />

{!! Form::label('Nuoroda*', '', ['class'=>'control-label', 'data-comment'=>' (Nieko neįvedus - automatiškai pagal pavadinimą; unikalus)']) !!}
{!! Form::text('slug', @$slug, ['class'=>'form-control multi']) !!}<br/>

{!! Form::label('Tekstas', '', ['class'=>'control-label']) !!}
<span class="multi" name="body">{!! Form::textarea('body', @$body, ['class'=>'form-control redactor']) !!}</span><br />

<div style="width: 150%">
    <div class="file-loading">
        <input id="gallery" type="file" multiple>
    </div>
</div><br />
<p class="alert alert-danger" data-btn-submit-error="" style="display: none">Pridėta naujų nuotraukų. Pirmiausia jas reikia įkelti arba pašalinti.</p>
{!! Form::submit('Siųsti', ['class'=>'btn btn-info btn-lg', 'data-btn-submit' => '']) !!}

@section('header')
    <link href="/gallery/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
@endsection

@section('footer')
    <script src="/gallery/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="/gallery/js/plugins/piexif.js" type="text/javascript"></script>
    <script src="/gallery/js/plugins/sortable.js" type="text/javascript"></script>
    <script src="/gallery/js/fileinput.js" type="text/javascript"></script>
    <script src="/gallery/js/lt.js" type="text/javascript"></script>
    <script src="/gallery/themes/fas/theme.js" type="text/javascript"></script>
    <script>
        $(document).on("ready", function() {
        $("#gallery").fileinput({
            'theme': 'fas',
            'uploadUrl': '{{ action('GalleryController@uploadImages') }}',
            uploadExtraData: {
                'itemId': '{{ (isset($item))?$item->id:0 }}',
                'fakeId': '{{ $fakeId }}'
            },
            allowedFileTypes: ['image'],
            showCaption: false,
            language: 'lt',
            browseClass: 'btn btn-default btn-secondary',
            uploadClass: 'btn btn-default btn-secondary btn-warning btn-lg ',
            fileActionSettings: {
                showRemove: true,
                showUpload: false,
                showDownload: false,
                showZoom: false,
                showDrag: true,
            },
            overwriteInitial: false,
            initialPreviewAsData: true,
            @if(isset($item))
            initialPreview: [
                @foreach($item->images()->orderBy('priority')->get() as $one)
                "{{ $one->imgUrl }}",
                @endforeach
            ],
            initialPreviewConfig: [
                    @foreach($item->images()->orderBy('priority')->get() as $k => $one)
                {caption: "{{ $one->photo }}", size: "{{ $one->imgSize }}", width: "120px",
                    url: "{{ action('GalleryController@deleteImage', $one->id) }}", key: "{{ $one->priority }}", id: "{{ $one->id }}"},
                @endforeach
            ]
            @endif
        }).on('fileuploaded', function(event, previewId, index, fileId) {
            $('.fileinput-upload-button, .fileinput-remove-button').hide();
        }).on('fileselect', function(event, numFiles, label) {
            $('.fileinput-upload-buttonv, .fileinput-remove-button').show();
        }).on('filesorted', function(event, params) {
            console.log('File sorted ', params.previewId, params.oldIndex, params.newIndex, params.stack);
            $.post('{{ action('GalleryController@priorityImage') }}', {images: params.stack});
        });


        $('[data-btn-submit]').click(function(){
            var galleryFilesCount = $('#gallery').fileinput('getFilesCount');
            if(galleryFilesCount > 0) {
                $('[data-btn-submit-error]').show();
                return false;
            }
        });

        });
    </script>
@endsection