<?php

use Illuminate\Database\Seeder;

class GalleryPluginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'Gallery kategorija',
            'slug' => 'gallery',
            'priority' => DB::table('categories')->max('priority')+1
        ]);
        DB::table('plugins')->insert([
            'name' => 'Gallery',
            'slug' => 'Gallery',
            'settings' => json_encode(['category_id' => DB::table('categories')->max('id')])
        ]);


        //test item
        DB::table('galleries')->insert([
            'id' => 1,
            'name' => 'Testas gallery',
            'slug' => 'testas-gallery',
            'body' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>'
        ]);

        //multilang
        $multiLang = new App\MultiLang();
        if($multiLang->isMultiLang()) {
            //category
            DB::table('trans')->insert([
                'lang' => 'en',
                'item_module' => 'category',
                'item_id' => DB::table('categories')->max('id'),
                'item_field' => 'name',
                'body' => 'Gallery category'
            ]);
            DB::table('trans')->insert([
                'lang' => 'en',
                'item_module' => 'category',
                'item_id' => DB::table('categories')->max('id'),
                'item_field' => 'slug',
                'body' => 'gallery-category'
            ]);
            //blog. test item
            DB::table('trans')->insert([
                'lang' => 'en',
                'item_module' => 'gallery',
                'item_id' => DB::table('galleries')->max('id'),
                'item_field' => 'name',
                'body' => 'Test gallery'
            ]);
            DB::table('trans')->insert([
                'lang' => 'en',
                'item_module' => 'gallery',
                'item_id' => DB::table('galleries')->max('id'),
                'item_field' => 'slug',
                'body' => 'test-gallery'
            ]);
            DB::table('trans')->insert([
                'lang' => 'en',
                'item_module' => 'gallery',
                'item_id' => DB::table('galleries')->max('id'),
                'item_field' => 'body',
                'body' => '<p>EN Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>'
            ]);
        }
    }
}
