<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('galleries', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('slug')->nullable();
            $table->text('body')->nullable();
            $table->string('photo')->nullable();
            $table->integer('priority')->nullable();

            $table->timestamps();
        });

        Schema::create('galleries_images', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('gallery_id');
            $table->string('photo');
            $table->integer('priority')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('galleries');
        Schema::drop('galleries_images');
    }
}
