<?php 

namespace App\Http\Controllers; 

use App\GalleriesImage;
use App\Priority;
use App\TranHelper;
use Request;
use App\Gallery;
use App\Tran;
use App\Image;
use Illuminate\Validation\Validator;
use App\Http\Requests\GalleryRequest;
use Response;

use Illuminate\Support\Str;

class GalleryController extends Controller
{
    public function __construct()
    {
        $this->tranHelper = new TranHelper();
    }

    //admin. list
    public function index()
    {
        if(Request::input('sortable')) {
            $items = Gallery::orderby("priority")->paginate(9999);
        } else {
            $items = Gallery::orderby("priority")->paginate(50);
        }
       	return view('admin/gallery/index', compact("items"));
    }

    //admin. create new
    public function create($id=0)
    {
        return view('admin/gallery/create', compact('id'));
    }

    //admin. save create
    public function store(GalleryRequest $request)
    {
        $item = Gallery::create($request->all());
        $item->update(["slug" => Str::slug($request->input("slug"))]);
        $priority = new Priority(Gallery::whereNotNull('id'), $item);
        $priority->addPriority();
        GalleriesImage::where('gallery_id', $request->input("fakeId"))->update(['gallery_id' => $item->id]);
        //if multi lang save values
        $this->tranHelper->setItemTranslations($item->id, Request::segment(2));
        return redirect()->action('GalleryController@index');
    }

    //admin. edit
    public function edit($id)
    {
        $item = Gallery::findorFail($id);
        return view('admin/gallery/edit', compact('item'));
    }

    //admin. save edit
    public function update($id, GalleryRequest $request)
    {
        $item = Gallery::findorFail($id);
        $item->update($request->all());
        $item->update(["slug" => Str::slug($request->input("slug"))]);
        //if multi lang save values
        $this->tranHelper->setItemTranslations($item->id, Request::segment(2));
        return redirect()->action('GalleryController@index');
    }

    //admin. destroy
    public function destroy($id)
    {
        $item = Gallery::findorFail($id);
        //delete images
        $deleteImages = $item->images()->pluck('id');
        foreach ($deleteImages as $del) {
            if ($del) {
                $img = GalleriesImage::find($del);
                Image::removeImage($img->photo, Gallery::IMAGES_PATH);
                $img->delete();
            }
        }
        //if multi lang remove values
        $this->tranHelper->removeItemTranslations($item->id, Request::segment(2));
        $item->delete();
        //check weight
        $priority = new Priority(Gallery::whereNotNull('id'), $item);
        $priority->fixPriorities();
        return redirect()->action('GalleryController@index');
    }

    //no used, but laravel should has it for resource route
    public function show()
    {
        exit();
    }

    public function uploadImages() {
        $photoFile = Request::file('file_data');
        $imageName = str_replace(' ', '_', Request::input('fileId'));
        move_uploaded_file($photoFile, public_path().Gallery::IMAGES_PATH.'/'.$imageName);
        if(Request::input('itemId') > 0) {
            $item = Gallery::find(Request::input('itemId'));
            $priority = $item->images()->max('priority') + 1;
            $image = new GalleriesImage(['photo' => $imageName, 'priority' => $priority]);
            $item->images()->save($image);
        } else {
            $priority = GalleriesImage::where('gallery_id', Request::input('fakeId'))->max('priority') + 1;
            $image = GalleriesImage::create(['gallery_id' => Request::input('fakeId'), 'photo' => $imageName, 'priority' => $priority]);
        }
        return [
            'initialPreview' => $image->imgUrl,
            'initialPreviewConfig' => [
                [
                    'type' => 'image',
                    'caption' => $image->photo,
                    'key' => $image->priority,
                    'size' => $image->imgSize,
                    'url' => action('GalleryController@deleteImage', $image->id),
                    'id' => $image->id,
                ]
            ],
            'append' => true
        ];
    }

    public function deleteImage($id) {
        $item = GalleriesImage::find($id);
        Image::removeImage($item->photo, Gallery::IMAGES_PATH);
        $galleryId = $item->id;
        $item->delete();
        return [];
    }

    public function priorityImage() {
        $priority = 1;
        foreach (Request::input('images') as $info) {
            $item = GalleriesImage::find($info['id']);
            if($item) {
                $item->update(['priority' => $priority]);
                $priority++;
            }
        }
    }
}
