<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;
use Request;
use App\Image;
use App;

class Gallery extends Model
{
    //no guarded, cause multi lang has unnecessary fields
    protected $fillable = ['name', 'slug', 'body'];

    //images gallery path
    const IMAGES_PATH = '/images/gallery/';

    //RALATIONSHIPS. one gallery have multi img
    public function images()
    {
        return $this->hasMany('App\GalleriesImage');
    }


    //site category
    static public function siteCategoryId() {
        $plugin = App\Plugin::where('slug', 'gallery')->first();
        return $plugin->category_id;
    }

    //get img html atrribute [$item->img]
    protected function getImgAttribute()
    {
        $item = $this->images()->orderBy('priority')->first();
        if($item) {
            return $item->img;
        }
        return NULL;
    }

    //get link attribute [$item->link]
    protected function getLinkAttribute()
    {
        $galleryHelper = new GalleryHelper();
        return $galleryHelper->getUrlByLang($this);
    }

    //prepare translation
    public function translate($tranHelper = null)
    {
        $tranHelper = ($tranHelper) ? $tranHelper : new TranHelper();
        return $tranHelper->getTranslatedItem($this, 'gallery');
    }
}

class GalleryHelper {

    /**
     * @var MultiLang|null
     */
    public $multiLang = null;

    /**
     * @var TranHelper|null
     */
    public $tranHelper = null;

    /**
     * CategoryHelper constructor.
     *
     * @param MultiLang|null $multiLang
     */
    public function __construct(MultiLang $multiLang = null)
    {
        $this->multiLang = ($multiLang) ? $multiLang : new MultiLang();
        $this->tranHelper = new TranHelper($this->multiLang);
        $this->categoryHelper = new CategoryHelper($this->multiLang);
    }

    /**
     * Get current Gallery item
     *
     * @return |null
     */
    public function getCurrent()
    {
        $lang = App::getLocale();
        //check max level url paths
        $i = Category::MAX_LEVEL+2;
        while ($i > 0) {
            $slug = Request::segment($i);
            if ($this->multiLang->isMultiLang() && !$this->multiLang->isDefaultLang()) {
                $transItems = Tran::where('lang', $lang)->where("item_module", "Gallery")
                    ->where("item_field", "slug")->where("body", $slug)->pluck('item_id')->toArray();
                $categories = Gallery::wherein('id', $transItems)->get();
            } else {
                $categories = Gallery::where('slug', $slug)->get();
            }
            foreach ($categories as $item) {
                $categoryId = Gallery::siteCategoryId();
                if($item->category_id>0) {
                    $categoryId = $item->category_id;
                }
                $url = substr($this->categoryHelper->getUrlByLang($categoryId).'/'.$item->translate()->slug, 1);
                if (Request::is($url . '*')) {
                    return $item;
                }
            }
            $i--;
        }
        return NULL;
    }

    /**
     * Get Gallery url by lang
     *
     * @param $item
     *
     * @return string
     */
    public function getUrlByLang($item)
    {
        $categoryId = Gallery::siteCategoryId();
        if ($item->category_id > 0) {
            $categoryId = $item->category_id;
        }
        return $this->categoryHelper->getUrlByLang($categoryId) . '/' . $item->translate($this->tranHelper)->slug;
    }
}