<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use File;

class GalleriesImage extends Model
{
	protected $fillable  = ['photo', 'priority', 'gallery_id'];

    //RALATIONSHIPS. each img has one gallery
	public function gallery()
    {
        return $this->belongsTo('App\Http\Modules\Gallery');
    }

    //get img attribute [$item->images->link]
    protected function getImgAttribute()
    {
        $old = Gallery::IMAGES_PATH.$this->photo;
        if ($this->photo!="" and File::exists(public_path().$old)) {
            return "<img src='".$old."' alt='' class='img-thumbnail'  />";
        }
        return NULL;
    }
    
    //get img attribute [$item->images->imglink]
    protected function getImgLinkAttribute()
    {
        $old = Gallery::IMAGES_PATH.$this->photo;
        if ($this->photo!="" and File::exists(public_path().$old)) {
            return "<a href='".$old."' data-lightbox='gallery'><img src='".$old."' alt='' class='img-thumbnail'  /></a>";
        }
        return NULL;
    }

    //get img attribute [$item->images->imgUrl]
    protected function getImgUrlAttribute()
    {
        return Gallery::IMAGES_PATH.$this->photo;
    }

    protected function getImgSizeAttribute()
    {
        $old = Gallery::IMAGES_PATH.$this->photo;
        if ($this->photo!="" and File::exists(public_path().$old)) {
            return File::size(public_path().$old);
        }
        return NULL;
    }
}
