@extends('site.layout')

@section('content')

	@foreach($items as $item)
		<?php $item = $item->translate() ?>
		<article class="navbar navbar-default col-md-3">
			<h3>{{ $item->name }}</h3>
			<figure class="thumbnail">{!! $item->img !!}</figure>
			<a href="{{ $item->link }}">plačiau</a>
		</article>
	@endforeach
    <div class="clearfix"></div>
	
@endsection 