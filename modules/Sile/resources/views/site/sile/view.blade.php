@extends('site.layout')

@section('content')

	<article >
		<h1>{{ $pageItem->name }}</h1>
		{{ $pageItem->data }}
		<figure class="col-md-3 thumbnail">{!! $pageItem->img !!}</figure>

		{!! $pageItem->body !!}
	</article>
	<div class="clearfix"></div>

@endsection 