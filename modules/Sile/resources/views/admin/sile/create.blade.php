@extends('admin/layout')

@section('content')

	<div class="col-md-6 col-md-offset-3">
		<h1>{{ trans('admin.create_new') }}</h1>
		@include('errors/list') 
		{!! Form::open(array('action' => ['SileController@store'], 'files' => true)) !!}
			@include('admin/sile/form') 
		{!! Form::close() !!}
	</div>
	
@endsection