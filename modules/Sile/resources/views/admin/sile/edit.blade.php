@extends('admin/layout')

@section('content')

	<div class="col-md-6 col-md-offset-3">
		<h1>{{ trans('admin.edit') }}</h1>
		@include('errors/list')
		{!! Form::model($item, ['method' => 'PATCH', 'action' => ['SileController@update', $item->id], 'files' => true]) !!}
			@include('admin/sile/form')   
        {!! Form::close() !!}
	</div>
	
@endsection