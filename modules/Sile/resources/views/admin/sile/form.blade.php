@include('admin/langs') 

{!! Form::label(trans('admin.photo'), '', ['class'=>'control-label']) !!}
@if(isset($item) and $item->img!="")
    <span style="width:100px;display:inline-block" class="thumb">{!! $item->img !!}</span>
@endif
{!! Form::file('photo', ['class'=>'form-control']) !!}<br />

{!! Form::label(trans('admin.title').'*', '', ['class'=>'control-label']) !!}
{!! Form::text('name', @$name, ['class'=>'form-control multi']) !!}<br />

{!! Form::label(trans('admin.link').'*', '', ['class'=>'control-label', 'data-comment'=>trans('admin.link_info')]) !!}
{!! Form::text('slug', @$slug, ['class'=>'form-control multi']) !!}<br/>

{!! Form::label(trans('admin.body'), '', ['class'=>'control-label']) !!}
<span class="multi" name="body">{!! Form::textarea('body', @$body, ['class'=>'form-control redactor']) !!}</span><br />


{!! Form::submit(trans('admin.submit'), ['class'=>'btn btn-info btn-lg']) !!}
