@extends('admin/layout')

@section('content')

    <h1 class="pull-left">Sile</h1>
    <a href="{{ action("SileController@create") }}" class="btn btn-success pull-right">
        <i class="glyphicon glyphicon-plus"></i> {{ trans('admin.create_new') }}
    </a>
    <div class="clearfix"></div><br/>

    <div class="table-responsive">
        <table class="table table-hover " data-sortable="\App\Sile">
            <tr @if(Request::input('sortable')) class="sortable-disable" @endif>
            <tr>
                <th>#</th>
                <th>{{ trans('admin.photo') }}</th>
                <th>{{ trans('admin.title') }}</th>
                <th>{{ trans('admin.link') }}</th>
                <th width="150">
                    @if(Request::input('sortable'))
                        <a href="{{ action("SileController@index") }}" class="btn btn-blank pull-right"
                           title="{{ trans('admin.priority_stop') }}" style="cursor: pointer">
                            <i class="glyphicon glyphicon-move"></i>
                        </a>
                    @else
                        <a href="{{ action("SileController@index") }}?sortable=1" class="btn btn-warning pull-right"
                           title="{{ trans('admin.priority_start') }}">
                            <i class="glyphicon glyphicon-sort"></i>
                        </a>
                    @endif
                </th>
            </tr>
            @foreach($items as $item)

                <tr data-sortable-id="{{ $item->id }}">
                    <td>{{ $item->id }}</td>
                    <td class="thumb">{!! $item->img !!}</td>
                    <td>{{ $item->name }}</td>
                    <td><a href="{{ Request::root().$item->link }}"
                           target="_blank">{{ Request::root().$item->link }}</a></td>
                    <td>
                        <a href="{{ action("SileController@edit", $item->id) }}" class="btn btn-info" title="{{ trans('admin.edit') }}"><i
                                    class="glyphicon glyphicon-pencil"></i></a>
                        {!! Form::model($item, ['method' => 'DELETE', 'action' => ['SileController@destroy', $item->id], 'class'=>'del-form', 'onclick'=>"return confirm('".trans('admin.remove_confirm')."')", 'title'=>trans('admin.remove')]) !!}
                        {!! Form::submit(' X ', ['class'=>'btn btn-danger']) !!}
                        {!! Form::close() !!}
                        @if(Request::input('sortable'))
                            <span class="btn"><i class="glyphicon glyphicon-move"></i></span>
                        @endif
                    </td>
                </tr>

            @endforeach
        </table>
    </div>
    {!! $items->appends(['search' => Request::input('search')])->render() !!}
@endsection 