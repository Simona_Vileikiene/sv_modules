<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class SileRequest extends FormRequest
{
        /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        $rules = [
            'photo' => 'image',
            'name' => 'required'
        ];
        //rules if update
        if ($this->method() == 'PATCH') {
            $id = Request::segment(3);
            $rules['slug'] = 'required|unique:siles,slug,' . $id;
        } else {
            //rules if create
            $rules['slug'] = 'required|unique:siles,slug';
        }
        return $rules;
    }

    public function all($keys = null)
    {
        $input = parent::all();
        //if slug is empty, slug = name
        if ($input['slug'] == "") {
            $input['slug'] = $input['name'];
        }
        $this->replace($input);
        return parent::all();
    }

}