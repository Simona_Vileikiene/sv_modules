<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;
use Request;
use App\Image;
use App;

class Sile extends Model
{
    //no guarded, cause multi lang has unnecessary fields
    protected $fillable = ['name', 'slug', 'body'];

    //images gallery path
    const IMAGES_PATH = '/images/sile/';

    //site category
    //const SITE_CATEGORY_ID = 10;

    static public function siteCategoryId() {
        return App\Plugin::where('slug', 'sile')->first()->category_id;
    }

    //get img html atrribute [$item->img]
    protected function getImgAttribute()
    {
        return Image::getImgHtml($this->photo, Sile::IMAGES_PATH, $this->name);
    }

    //get link attribute [$item->link]
    protected function getLinkAttribute()
    {
        $sileHelper = new SileHelper();
        return $sileHelper->getUrlByLang($this);
    }

    //prepare translation
    public function translate()
    {
        return Tran::translate($this, 'sile');
    }
}

class SileHelper {

    /**
     * @var MultiLang|null
     */
    public $multiLang = null;

    /**
     * @var TranHelper|null
     */
    public $tranHelper = null;

    /**
     * CategoryHelper constructor.
     *
     * @param MultiLang|null $multiLang
     */
    public function __construct(MultiLang $multiLang = null)
    {
        $this->multiLang = ($multiLang) ? $multiLang : new MultiLang();
        $this->tranHelper = new TranHelper($this->multiLang);
        $this->categoryHelper = new CategoryHelper($this->multiLang);
    }

    /**
     * Get current sile item
     *
     * @return |null
     */
    public function getCurrent()
    {
        $lang = App::getLocale();
        //check max level url paths
        $i = Category::MAX_LEVEL+2;
        while ($i > 0) {
            $slug = Request::segment($i);
            if ($this->multiLang->isMultiLang() && !$this->multiLang->isDefaultLang()) {
                $transItems = Tran::where('lang', $lang)->where("item_module", "sile")
                    ->where("item_field", "slug")->where("body", $slug)->pluck('item_id')->toArray();
                $categories = Sile::wherein('id', $transItems)->get();
            } else {
                $categories = Sile::where('slug', $slug)->get();
            }
            foreach ($categories as $item) {
                $categoryId = Sile::siteCategoryId();
                if($item->category_id>0) {
                    $categoryId = $item->category_id;
                }
                $url = substr($this->categoryHelper->getUrlByLang($categoryId).'/'.$item->translate()->slug, 1);
                if (Request::is($url . '*')) {
                    return $item;
                }
            }
            $i--;
        }
        return NULL;
    }

    /**
     * Get sile url by lang
     *
     * @param $item
     *
     * @return string
     */
    public function getUrlByLang($item)
    {
        $categoryId = Sile::siteCategoryId();
        if ($item->category_id > 0) {
            $categoryId = $item->category_id;
        }
        return $this->categoryHelper->getUrlByLang($categoryId) . '/' . $item->translate($this->tranHelper)->slug;
    }
}