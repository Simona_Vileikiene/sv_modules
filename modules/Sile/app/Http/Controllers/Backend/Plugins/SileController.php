<?php 

namespace App\Http\Controllers; 

use App\Priority;
use App\TranHelper;
use Request;
use App\Sile;
use App\Tran;
use App\Image;
use Illuminate\Validation\Validator;
use App\Http\Requests\SileRequest;
use Response;

use Illuminate\Support\Str;

class SileController extends Controller 
{
    public function __construct()
    {
        $this->tranHelper = new TranHelper();
    }

    //admin. list
    public function index()
    {
        if(Request::input('sortable')) {
            $items = Sile::orderby("priority")->paginate(9999);
        } else {
            $items = Sile::orderby("priority")->paginate(50);
        }
       	return view('admin/sile/index', compact("items"));
    }

    //admin. create new
    public function create($id=0)
    {
        return view('admin/sile/create', compact('id'));
    }

    //admin. save create
    public function store(SileRequest $request)
    {
        $item = Sile::create($request->all());
        $item->update(["slug" => Str::slug($request->input("slug"))]);
        $priority = new Priority(Sile::whereNotNull('id'), $item);
        $priority->addPriority();
        //upload new image
        if($request->file('photo')) {
            $imageName = Image::uploadImage($item, $request->file('photo'), Sile::IMAGES_PATH);
            $item->photo = $imageName;
            $item->save();
        }
        //if multi lang save values
        $this->tranHelper->setItemTranslations($item->id, Request::segment(2), $request);
        return redirect()->action('SileController@index'); 
    }

    //admin. edit
    public function edit($id)
    {
        $item = Sile::findorFail($id);
        return view('admin/sile/edit', compact('item'));
    }

    //admin. save edit
    public function update($id, SileRequest $request)
    {
        $item = Sile::findorFail($id);
        $item->update($request->all());
        $item->update(["slug" => Str::slug($request->input("slug"))]);
        //delete old and upload new image
        if($request->file('photo')) {
            Image::removeImage($item->photo, Sile::IMAGES_PATH);
            $imageName = Image::uploadImage($item, $request->file('photo'), Sile::IMAGES_PATH);
            $item->photo = $imageName;
            $item->save();
        }
        //if multi lang save values
        $this->tranHelper->setItemTranslations($item->id, Request::segment(2), $request);
        return redirect()->action('SileController@index'); 
    }

    //admin. destroy
    public function destroy($id)
    {
        $item = Sile::findorFail($id);
        Image::removeImage($item->photo, Sile::IMAGES_PATH);
        $this->tranHelper->removeItemTranslations($item->id, Request::segment(2));
        $item->delete();
        //check weight
        $priority = new Priority(Sile::whereNotNull('id'));
        $priority->fixPriorities();
        return redirect()->action('SileController@index'); 
    }

    //admin. up weight
    public function up($id)
    {
        $item = Sile::findorFail($id);
        Priority::updatePriority(Sile::class, $item);
        return redirect()->action('SileController@index');
    }

    //admin. down weight
    public function down($id)
    {
        $item = Sile::findorFail($id);
        Priority::updatePriority(Sile::class, $item, false);
        return redirect()->action('SileController@index');
    }

    //no used, but laravel should has it for resource route
    public function show()
    {
        exit();
    }

}
