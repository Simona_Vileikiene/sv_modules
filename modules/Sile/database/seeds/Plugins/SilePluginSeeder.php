<?php

use Illuminate\Database\Seeder;

class SilePluginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plugins')->insert([
            'name' => 'Sile',
            'slug' => 'Sile',
            'category_id' => 10
        ]);
        DB::table('categories')->insert([
            'id' => 10,
            'name' => 'Sile kats.',
            'slug' => 'sile'
        ]);
    }
}
