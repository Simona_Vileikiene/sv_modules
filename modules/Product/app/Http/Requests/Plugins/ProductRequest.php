<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Http\Request;


class ProductRequest extends FormRequest
{
        /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
        ];
        if ($this->method()=='PATCH') {
            $id = Request::segment(3);
            $rules['code'] = 'required|unique:products,code,'.$id;
            $rules['slug'] = 'required|unique:products,slug,'.$id;
        } else {
            //rules if create
            $rules['code'] = 'required|unique:products,code';
            $rules['slug'] = 'required|unique:products,slug';
        }
        $rules = array_merge($rules, $this->productCombinationsValidation());
        $rules = array_merge($rules, $this->productCombinationsValidation('combinations_new'));
        return $rules;
    }

    public function all($keys = null)
    {
        $input = parent::all();
        //if slug is emptry, slug = name
        if ($input['slug']=="") {
            $input['slug'] = $input['name'];
        }
        $this->replace($input);
        return parent::all();
    }

    public function attributes()
    {
        $attributes = [];
        $attributes = array_merge($attributes, $this->productCombinationsValidation(
            'combinations', true
        ));
        $attributes = array_merge($attributes, $this->productCombinationsValidation(
            'combinations_new', true
        ));
        return $attributes;
    }

    protected function productCombinationsValidation($fieldName = 'combinations', $isAttributes = false) {
        $items = [];
        if(Request::input($fieldName)) {
            foreach (Request::input($fieldName) as $k => $one) {
                if (!empty($one['price']) || !empty($one['qty'])) {
                    if (\App\Product::COMBINATIONS) {
                        $combinationName = '#' . $k . ' kombinacijos ';
                    } else {
                        $combinationName = '';
                    }
                    if ($isAttributes) {
                        $items[$fieldName . '.' . $k . '.price'] = $combinationName . 'Kaina';
                        $items[$fieldName . '.' . $k . '.sale'] = $combinationName . 'Akcijinė (nauja) Kaina';
                        if(\App\Product::USE_WAREHOUSE) {
                            $items[$fieldName . '.' . $k . '.qty'] = $combinationName . 'Kiekis';
                        }
                    } else {
                        $items[$fieldName . '.' . $k . '.price'] = 'required|numeric';
                        if (!empty($one['sale'])) {
                            $items[$fieldName . '.' . $k . '.sale'] = 'numeric';
                        }
                        if(\App\Product::USE_WAREHOUSE) {
                            $items[$fieldName . '.' . $k . '.qty'] = 'required|numeric';
                        }
                    }
                }
            }
        }
        return $items;
    }
}