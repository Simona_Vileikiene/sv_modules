<?php

namespace App\Http\Controllers;

use App\CategoryHelper;
use App\Priority;
use App\ProductsCombination;
use App\TranHelper;
use Request;
use Illuminate\Support\Str;

use App\Category;
use App\Product;
use App\Image;
use App\ProductsCategory;
use App\ProductsImage;
use App\Tran;

use App\Http\Requests\ProductRequest;

class ProductController extends Controller
{
    /**
     * CategoryController constructor.
     */
    public function __construct()
    {
        $this->categoryHelper = new CategoryHelper();
        $this->tranHelper = new TranHelper();
    }

    //admin. list
    public function index()
    {
        $items = Product::whereNotNull('id');
        if(Request::input('sortable')) {
            $items = $items->orderby("priority")->paginate(9999);
        } else {
            if (Request::input('category') > 0) {
                $categoriesList = ProductsCategory::where('category_id', Request::input('category'))->pluck('product_id');
                $items = $items->whereIn('id', $categoriesList);
            }
            if (!empty(Request::input('search'))) {
                $items = $items->where(function ($query) {
                    $query->where('name', 'like', '%' . Request::input('search') . '%')
                        ->orwhere('code', 'like', '%' . Request::input('search') . '%');
                });
            }
            $items = $items->orderby("priority")->paginate(50);
        }
        $categories = $this->categoryHelper->getAllSubcategoriesList(Product::siteCategoryId());
        return view('admin/product/index', compact("items", 'categories'));
    }

    //admin. create new
    public function create($id = 0)
    {
        $categories = $this->categoryHelper->getAllSubcategoriesList(Product::siteCategoryId());
        return view('admin/product/create', compact('id', 'categories'));
    }

    //admin. save create
    public function store(ProductRequest $request)
    {
        $item = Product::create($request->all());
        $item->update(["slug" => Str::slug($request->input("slug"))]);
        $priority = new Priority(Product::whereNotNull('id'), $item);
        $priority->addPriority();
        ProductsCategory::manageProductCategories($item, $request);
        ProductsImage::manageProductImages($item, $request);
        ProductsCombination::manageProductCombinations($item, $request);
        //if multi lang save values
        $this->tranHelper->setItemTranslations($item->id, Request::segment(2), $request);
        return redirect()->action('ProductController@index');
    }

    //admin. edit
    public function edit($id)
    {
        $item = Product::findorFail($id);
        $categoryHelper = new CategoryHelper();
        $categories = $this->categoryHelper->getAllSubcategoriesList(Product::siteCategoryId());
        return view('admin/product/edit', compact('item', 'categories'));
    }

    //admin. save edit
    public function update($id, ProductRequest $request)
    {
        $item = Product::findorFail($id);
        $item->update($request->all());
        $item->update(["slug" => Str::slug($request->input("slug"))]);
        ProductsCategory::manageProductCategories($item, $request);
        ProductsImage::manageProductImages($item, $request);
        ProductsCombination::manageProductCombinations($item, $request);
        //if multi lang save values
        $this->tranHelper->setItemTranslations($item->id, Request::segment(2), $request);
        return redirect()->action('ProductController@index');
    }

    //admin. destroy
    public function destroy($id)
    {
        $item = Product::findorFail($id);
        ProductsCategory::manageProductCategories($item);
        ProductsImage::manageProductImages($item, null, true);
        ProductsCombination::manageProductCombinations($item, null, true);
        $this->tranHelper->removeItemTranslations($item->id, Request::segment(2));
        $item->delete();
        $priority = new Priority(Product::whereNotNull('id'), $item);
        $priority->fixPriorities();
        return redirect()->action('ProductController@index');
    }

    //no used, but laravel should has it for resource route
    public function show()
    {
        exit();
    }
}
