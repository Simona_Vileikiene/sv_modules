<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use File;

class ProductsCategory extends Model
{
	protected $fillable  = ['category_id', 'product_id'];

    //RALATIONSHIPS. each img has one product
	public function product()
    {
        return $this->belongsTo('App\Product');
    }

    //RALATIONSHIPS. each img has one product
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    //manage product categories
    static function manageProductCategories($item, $request = NULL) {
        if($item->categories()->count()>0) {
            $item->categories()->delete();
        }
        //add new
        if(!empty($request) && is_array($request->input('categories'))) {
            foreach ($request->input('categories') as $one) {
                $item->categories()->save(new ProductsCategory(['category_id' => $one]));
            }
        }
    }
}
