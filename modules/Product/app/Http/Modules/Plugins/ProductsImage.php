<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use File;

class ProductsImage extends Model
{
	protected $fillable  = ['photo', 'priority'];

    //RALATIONSHIPS. each img has one product
    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    //get img attribute [$item->images->link]
    protected function getImgAttribute()
    {
        return Image::getImgHtml($this->photo, Product::IMAGES_PATH, $this->product->name);
    }

    //get img attribute [$item->images->imglink]
    protected function getImgLinkAttribute()
    {
        $old = Product::IMAGES_PATH.$this->photo;
        if ($this->photo!="" and File::exists(public_path().$old)) {
            return "<a href='".$old."' data-lightbox='gallery'>".$this->img."</a>";
        }
        return NULL;
    }

    //manage product images
    static function manageProductImages($item, $request = NULL, $deleteAll = false) {
        $deleteImages = [];
        if($deleteAll) {
            //delete all product images
            $deleteImages = $item->images()->pluck('id');
        } else {
            //delete checked imgs
            if($request->has('photos_del')) {
                $deleteImages = $request->input('photos_del');
            }
        }
        if(count($deleteImages)>0) {
            foreach ($deleteImages as $del) {
                if ($del) {
                    $img = ProductsImage::find($del);
                    Image::removeImage($img->photo, Product::IMAGES_PATH);
                    $img->delete();
                }
            }
        }
        //add new
        if(!empty($request) && $request->file('photos')) {
            foreach ($request->file('photos') as $photo) {
                $imageName = Image::uploadImage($item, $photo, Product::IMAGES_PATH);
                $priority = $item->images()->max('priority') + 1;
                $item->images()->save(new ProductsImage(['photo' => $imageName, 'priority' => $priority]));
            }
        }
    }

}
