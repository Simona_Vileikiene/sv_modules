<?php

namespace App;

use App;
use Illuminate\Support\Str;
use Request;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //no guarded, cause multi lang has unnecessary fields
    protected $fillable = ['code', 'name', 'slug', 'body'];

    //images gallery path
    const IMAGES_PATH = '/images/product/';

    //has combinations: true, false
    const COMBINATIONS = false;

    //product buy mode: false, 'cart', 'order'
    const BUY_MODE = false;

    //use warehouse (qty): true, false
    const USE_WAREHOUSE = true;

    static public function siteCategoryId() {
        $item = Category::where('plugin', 'product')->first();
        if(!$item) {
            throw new \Exception("Plugin don't have a category.");
        }
        return $item->id;
    }

    //RALATIONSHIPS. one product have multi img
    public function images()
    {
        return $this->hasMany('App\ProductsImage');
    }

    //RALATIONSHIPS. one product have multi categories
    public function categories()
    {
        return $this->hasMany('App\ProductsCategory');
    }

    //RALATIONSHIPS. one product have multi combinations
    public function combinations()
    {
        return $this->hasMany('App\ProductsCombination');
    }

    //get img attribute [$item->images->link]
    protected function getCategoriesNamesAttribute()
    {
        $items =[];
        foreach ($this->categories()->get() as $item) {
            $items[] = $item->category->name;
        }
        return $items;
    }

    //get link attribute [$item->link]
    protected function getLinkAttribute()
    {
        $categoryId = Product::siteCategoryId();
        if($this->categories()->first()) {
            $categoryId = $this->categories()->first()->category_id;
        }
        return Product::getProductLinkByCategory($this, $categoryId);
    }

    //get product link by category
    static function getProductLinkByCategory($item, $categoryId) {
        $lang = \App::getLocale();
        $categoryHelper = new CategoryHelper();
        return $categoryHelper->getUrlByLang($categoryId, $lang).'/'.$item->slug;
    }

    //get Description [$item->description]
    protected function getDescriptionAttribute() {
        return Str::limit(strip_tags($this->body), 120);
    }

    //get img attribute [$item->img]
    protected function getImgAttribute()
    {
        $item = $this->images()->orderBy('priority')->first();
        if($item) {
            return $item->img;
        }
        return "<img src='/images/noimage.png' alt='".$this->name."' class='img-thumbnail' />";
    }

    //get price attribute [$item->price]
    protected function getPriceAttribute()
    {
        $item = $this->combinations()->first();
        if($item) {
            if(!empty($item->sale)) {
                return $item->sale;
            }
            return $item->price;
        }
        return NULL;
    }

    //get old attribute [$item->PriceOld]
    protected function getPriceOldAttribute()
    {
        $item = $this->combinations()->first();
        if($item) {
            if(!empty($item->sale)) {
                return $item->price;
            }
        }
        return NULL;
    }

    //get old attribute [$item->qty]
    protected function getQtyAttribute()
    {
        $item = $this->combinations()->first();
        if($item) {
            return $item->qty;
        }
        return NULL;
    }

    //get old attribute [$item->CombinationId]
    protected function getCombinationIdAttribute()
    {
        $item = $this->combinations()->first();
        if($item) {
            return $item->id;
        }
        return NULL;
    }

    //get current product by url and lang
    public static function getCurrent()
    {
        $multiLang = new MultiLang();
        $categoryHelper = new CategoryHelper();
        $lang = App::getLocale();
        //check max level url paths
        $i = Category::MAX_LEVEL+2;
        while ($i > 0) {
            $slug = Request::segment($i);
            if (!$multiLang->isDefaultLang()) {
                $transItems = Tran::where('lang', $lang)->where("item_module", "product")
                    ->where("item_field", "slug")->where("body", $slug)->pluck('item_id')->toArray();
                $categoryByAutoSlug = explode($multiLang->currentLang.'-product-', $slug);
                if(isset($categoryByAutoSlug[1])) {
                    $transItems[] = $categoryByAutoSlug[1];
                }
                $categories = Product::wherein('id', $transItems)->get();
            } else {
                $categories = Product::where('slug', $slug)->get();
            }
            foreach ($categories as $item) {
                $categoryId = Product::siteCategoryId();
                if($item->categories()->first()) {
                    $categoryId = $item->categories()->first()->category_id;
                }
                $url = substr($categoryHelper->getUrlByLang($categoryId, $lang).'/'.$item->slug, 1);
                if (Request::is($url . '*')) {
                    return $item;
                }
            }
            $i--;
        }
        return NULL;
    }

    //prepare translation
    public function translate($tranHelper = null)
    {
        $tranHelper = ($tranHelper) ? $tranHelper : new TranHelper();
        return $tranHelper->getTranslatedItem($this, 'product');
    }
}
