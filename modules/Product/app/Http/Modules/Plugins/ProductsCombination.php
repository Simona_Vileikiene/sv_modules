<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Request;

use App\Http\Modules\Product;

class ProductsCombination extends Model
{
    protected $fillable = ['product_id', 'name', 'price', 'sale', 'qty'];

    //RALATIONSHIPS. each img has one product
    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    //get price true attribute [$item->price]
    protected function getPriceTrueAttribute()
    {
        if (!empty($this->sale)) {
            return $this->sale;
        }
        return $this->price;
    }

    //manage product combinations
    static function manageProductCombinations($item, $request = NULL, $deleteAll = false)
    {
        //delete all product combinations
        if ($deleteAll) {
            $item->combinations()->delete();
        }
        if (!empty($request) && $request->input('combinations')) {
            foreach ($request->input('combinations') as $one) {
                $combination = ProductsCombination::find($one['id']);
                if (isset($one['delete']) && $one['delete']== 1) {
                    $combination->delete();
                } else {
                    $combination->update([
                        'price' => $one['price'],
                        'sale' => $one['sale'],
                        'qty' => (\App\Product::USE_WAREHOUSE)?$one['qty']:0
                    ]);
                    if (isset($one['name'])) {
                        $combination->update([
                            'name' => $one['name']
                        ]);
                    }
                }
            }
        }
        //add new
        if (!empty($request) && $request->input('combinations_new')) {
            foreach ($request->input('combinations_new') as $one) {
                if (!empty($one['price']) || !empty($one['qty'])) {
                    $combination = new ProductsCombination([
                        'price' => $one['price'],
                        'sale' => $one['sale'],
                        'qty' => (\App\Product::USE_WAREHOUSE)?$one['qty']:0
                    ]);
                    if (isset($one['name'])) {
                        $combination->update([
                            'name' => $one['name']
                        ]);
                    }
                    $item->combinations()->save($combination);
                }
            }
        }
    }
}
