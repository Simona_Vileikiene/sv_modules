@extends('admin/layout')

@section('content')

    <div class="row">
        <h1 class="col-lg-9 text-lg-left text-center">{{ __('Produktai') }}</h1>
        <div class="col-lg-3 text-right p-2">
            <a href="{{ action("ProductController@create") }}" class="btn btn-outline-success">
                <i class="fas fa-plus"></i> {{ trans('admin.create_new') }}
            </a>
        </div>
    </div>
    {!! Form::open(array('method' => 'get', 'action' => 'ProductController@index', 'class'=>'row mb-4')) !!}
        <div class="col-lg-3">
            {!! Form::select('category', $categories, Request::input('category'), ['class'=>'form-control']) !!}
        </div>
        <div class="col-lg-3 p-0">
            {!! Form::text('search', Request::input('search'), ['class'=>'form-control', 'placeholder'=>'Kodas, Pavadinimas']) !!}
        </div>
        <div class="col-lg-2">
            {!! Form::submit('Paieška', ['class'=>'btn btn-outline-main']) !!}
            @if(Request::input('category')>0 or Request::input('search')!='')
                <a href="{{ action("ProductController@index") }}" class="btn btn-outline-main">Išvalyti</a>
            @endif
        </div>
    {!! Form::close() !!}

	<div class="table-responsive">
        <table class="table table-hover actions-2" @if(Request::input('sortable')) data-sortable="\App\Product" @endif>
            <tr @if(Request::input('sortable')) class="sortable-disable" @endif>
				<th>#</th>
				<th>Nuotrauka</th>
				<th>Kodas</th>
				<th>Pavadinimas</th>
				<th>Nuoroda</th>
				@if(count($categories)>2)
					<th>Kategorija</th>
				@endif
				<th>Kaina</th>
				@if(\App\Product::USE_WAREHOUSE)
					<th>Kiekis</th>
				@endif
				<th width="150">
                    <a href="{{ action("ProductController@index") }}{{ (Request::input('sortable'))?'':'?sortable=1' }}"
                       class="btn btn-sm btn-outline-warning text-nowrap"
                       title="{{ (Request::input('sortable'))?'Sustabdyti':'Rikiuoti' }}">
                        <i class="fas fa-sort"></i> {{ (Request::input('sortable'))?'Sustabdyti':'Rikiuoti' }}
                    </a>
				</th>
			</tr>
			@foreach($items as $item)
				<tr data-sortable-id="{{ $item->id }}">
					<td>{{ $item->id }}</td>
					<td class="thumb">{!! $item->img !!}</td>
					<td>{{ $item->code }}</td>
					<td>{{ $item->name }}</td>
					<td><a href="{{ $item->link }}" target="_blank">{{ Request::root() }}{{ $item->link }}</a></td>
					@if(count($categories)>2)
						<td>{{ implode(', ', $item->categories_names) }}</td>
					@endif
					<td>{{ $item->price }} @if($item->priceOld>0) <del>{{ $item->priceOld }}</del> @endif</td>
					@if(\App\Product::USE_WAREHOUSE)
						<td> {{ $item->qty }} </td>
					@endif
					<td>
                        <a href="{{ action("ProductController@edit", $item->id) }}" class="btn btn-sm btn-outline-info" title="Redaguoti"><i class="far fa-edit"></i></a>
                        {!! Form::model($item, ['method' => 'DELETE', 'action' => ['ProductController@destroy', $item->id], 'class'=>'d-inline', 'onclick'=>"return confirm('Ar tikrai norite ištrinti?')", 'title'=>'Ištrinti']) !!}
                        {!! Form::submit(' X ', ['class'=>'btn btn-sm btn-outline-danger']) !!}
                        {!! Form::close() !!}
						@if(Request::input('sortable'))
                            <span class="btn btn-sm"><i class="fas fa-arrows-alt"></i></span>
						@endif
					</td>
				</tr>

			@endforeach
		</table>
	</div>

	{!! $items->appends(['category' => Request::input('category'), 'search' => Request::input('search')])->render() !!}
@endsection
