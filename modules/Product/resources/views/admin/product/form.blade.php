<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">
            Pagrindinė informacija</a>
    </li>
    @if(App\Product::COMBINATIONS)
        <li role="presentation"><a href="#comb" aria-controls="comb" role="tab" data-toggle="tab">Kombinacijos</a></li>
    @endif
</ul>

<div class="tab-content">
    <div role="tabpanel" class="tab-pane active " id="home">
        {!! Form::label('Nuotraukos', '', ['class'=>'control-label', 'data-comment'=>' CTRL (galima pažymėti kelais)']) !!}
        @if(isset($item) and count($item->images)>0)
            <div id="product-images" data-sortable="\App\ProductsImage">
                @foreach($item->images()->orderBy('priority')->get() as $one)
                    <p data-sortable-id="{{ $one->id }}" class="d-inline-block mr-1">
                        <span class="thumb">{!! $one->img !!}</span><br />
                        <small class="text-muted">{!! Form::checkbox('photos_del[]', $one->id, false) !!} Trinti</small>
                    </p>
                @endforeach
            </div>
            {!! Form::hidden('copyimgs', $item->id) !!}
        @endif
        {!! Form::file('photos[]', ['class'=>'form-control', 'multiple'=>true]) !!}<br/>

        {!! Form::label('Kodas*', '', ['class'=>'control-label', 'data-comment'=>' (unikalus)']) !!}
        {!! Form::text('code', @$code, ['class'=>'form-control multi']) !!}<br/>

        {!! Form::label('Pavadinimas*', '', ['class'=>'control-label']) !!}
        {!! Form::text('name', @$name, ['class'=>'form-control multi']) !!}<br/>

        {!! Form::label('Nuoroda*', '', ['class'=>'control-label', 'data-comment'=>' (Nieko neįvedus - automatiškai pagal pavadinimą; unikalus)']) !!}
        {!! Form::text('slug', @$slug, ['class'=>'form-control multi']) !!}<br/>

        {!! Form::label('Tekstas', '', ['class'=>'control-label']) !!}
        <span class="multi"
              name="body">{!! Form::textarea('body', @$body, ['class'=>'form-control redactor']) !!}</span><br/>

        @if(count($categories)>2)
            {!! Form::label('Kategorijos*', '', ['class'=>'control-label']) !!}
            {!! Form::select('categories[]', $categories, (isset($item))?$item->categories()->pluck('category_id'):[], ['class'=>'form-control', 'multiple'=>'multiple' , 'size'=>5]) !!}
            <br/>
        @else
            {!! Form::hidden('categories[]', App\Product::siteCategoryId(), ['class'=>'form-control']) !!}
        @endif

        @if(!App\Product::COMBINATIONS)
            @if(isset($item) && count($item->combinations)>0)
                <?php $fieldName = 'combinations'; ?>
                <?php $combination = $item->combinations()->first(); $id = $combination->id; ?>
            @else
                <?php $fieldName = 'combinations_new'; ?>
                <?php $id = 1; $combination = NULL; ?>
            @endif
            @include('admin/product/combinations')
        @endif
    </div>

    @if(App\Product::COMBINATIONS)
        <div role="tabpanel" class="tab-pane fade" id="comb">
            @if(isset($item) && count($item->combinations)>0)
                <h3>Prekės kombinacijos</h3>
                <?php $fieldName = 'combinations'; ?>
                @foreach($item->combinations as $combination)
                    <?php $id = $combination->id ?>
                    @include('admin/product/combinations')
                @endforeach
            @endif
            <div id="combinations">
                <?php $fieldName = 'combinations_new'; ?>
                @for($i=1; $i<6; $i++)
                    <?php $id = $i; $combination = NULL; ?>
                    @include('admin/product/combinations')
                @endfor
            </div>
            <div class="col-md-12">
                <a href="#combination-new" class="btn btn-success pull-right" data-name="addNewCombination">
                    <i class="glyphicon glyphicon-plus"></i></a>
            </div>
            <div class="clearfix"></div>
        </div>
    @endif

</div>
{!! Form::submit('Siųsti', ['class'=>'btn btn-outline-main  btn-lg']) !!}
<br/> <br/>
