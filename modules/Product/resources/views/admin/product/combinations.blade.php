<div class="combination">
    @if(App\Product::COMBINATIONS)
        <div class="col-md-12">
            <b data-name="combination_no">#{{ $id  }}</b>
            {!! Form::label('Kombinacijos pavadinimas*', '', ['class'=>'control-label']) !!}
            {!! Form::text($fieldName.'['.$id.'][name]', ($combination)?$combination->name:old($fieldName.'['.$id.'][name]'),
            ['class'=>'form-control', 'data-name'=>'combination_name']) !!}
        </div>
    @endif
    <div class="col-md-6" style="padding-right:10px">
        {!! Form::label('Kaina*', '', ['class'=>'control-label', 'data-comment'=>' pvz. 10 arba 10.50 (su tašku)']) !!}
        {!! Form::text($fieldName.'['.$id.'][price]', ($combination)?$combination->price:old($fieldName.'['.$id.'][price]'),
        ['class'=>'form-control', 'data-name'=>'combination_price']) !!}
    </div>
    <div class="col-md-6">
        {!! Form::label('Akcijinė (nauja) kaina', '', ['class'=>'control-label', 'data-comment'=>' pvz. 10 arba 10.50']) !!}
        {!! Form::text($fieldName.'['.$id.'][sale]', ($combination)?$combination->sale:old($fieldName.'['.$id.'][sale]'),
        ['class'=>'form-control', 'data-name'=>'combination_sale']) !!}
    </div>
    @if(\App\Product::USE_WAREHOUSE)
        <div class="col-md-6" style="padding-right:10px">
            {!! Form::label('Kiekis*', '', ['class'=>'control-label']) !!}
            {!! Form::text($fieldName.'['.$id.'][qty]', ($combination)?$combination->qty:old($fieldName.'['.$id.'][qty]'),
            ['class'=>'form-control', 'data-name'=>'combination_qty']) !!}
        </div>
    @endif
    <div class="col-md-6" style="padding-top:30px">
        {!! Form::hidden($fieldName.'['.$id.'][id]', $id, false, ['class'=>'form-control radio']) !!}
        @if(App\Product::COMBINATIONS && $combination)
            {!! Form::checkbox($fieldName.'['.$id.'][delete]', 1, false, ['class'=>'form-control radio']) !!}
            <small>Pašalinti</small>
        @endif
    </div>
    <div class="clearfix"></div>
</div>

