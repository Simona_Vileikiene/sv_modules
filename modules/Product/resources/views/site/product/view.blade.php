@extends('site.layout')

@section('content')

    <article id="product-view" class="width col-sm-5">
        <h1>{{ $pageItem->name }}</h1>
        <div class="row">
            <div class="col-lg-4 p-0">
            <div id="images-view">
                @if($pageItem->images->count()==0)
                    <figure><img src="/site/images/blank.jpg" alt="{{ $pageItem->name }}" /></figure>
                @else
                    <figure>
                        @foreach($pageItem->images()->orderby('priority')->get() as $k=>$one)
                            <div>{!! $one->imglink !!}</div>
                        @endforeach
                    </figure>
                    <div class="thumbs row">
                        @foreach($pageItem->images()->orderby('priority')->get() as $k=>$one)
                            <div class="col-lg-4 col-sm-6 col-xs-6">{!! $one->img !!}</div>
                        @endforeach
                    </div>
                @endif
            </div>
            </div>
            <div class="col-lg-8">
                <b>Kaina:</b> <span>{{ $pageItem->price  }}</span> Eur
                @if($pageItem->priceOld>0) <del>{{ $pageItem->priceOld }}</del> Eur @endif

                @if($pageItem->body!="")
                    <div class="box about">{!! $pageItem->body !!}</div>
                @endif
                <br />

                @if(App\Product::USE_WAREHOUSE && empty($pageItem->qty))
                    <div class="alter alert-danger">Nėra sandėlyje</div>
                @elseif(App\Product::BUY_MODE == 'cart')
                    <a href="{{ action("Frontend\ShoppingCartController@add", [$pageItem->id, $pageItem->CombinationId, 1]) }}" class="btn btn-info" data-cart-add>Pirkti</a>
                @elseif(App\Product::BUY_MODE == 'order')
                    Užsakymas
                    @if (Session::get('orderMessage'))
                        <p class="alert alert-success">{{ Session::get('orderMessage') }}</p>
                    @elseif (Session::get('orderError'))
                        <p class="alert alert-danger">{{ Session::get('orderError') }}</p>
                    @else
                        @include('errors/list')
                        {!! Form::open(array('action' => ['Frontend\OrderController@orderStore', $pageItem->id, $pageItem->CombinationId, 1], 'files' => true)) !!}
                        @include('site/order/form')
                        {!! Form::submit('Siųsti', ['class'=>'btn btn-info btn-lg']) !!}
                        {!! Form::close() !!}
                    @endif
                @endif

            </div>
        </div>

    </article>


@endsection

@section('footer')
    <script>
        function selectImg(no) {
            $('#images-view figure div').hide();
            $('#images-view .thumbs div').removeClass('current');
            $('#images-view figure div:eq('+no+')').show();
            $('#images-view .thumbs div:eq('+no+')').addClass('current');
        }
        $('#images-view .thumbs div').click(function () {
            var no = $(this).index();
            selectImg(no);
        });
        selectImg(0);
    </script>
@endsection
