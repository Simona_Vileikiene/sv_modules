@extends('site.layout')

@section('content')
    <h1 class="h1">{{ $page->name  }}</h1>
    <div id="catalog">

        @foreach($items as $item)
            <?php $item = $item->translate() ?>
            <article class="product col-sm-4">

                <figure class="thumbnail"><a href="{{ $item->link }}">@if($item->img=="")<img src="/site/images/blank.jpg" alt="{{ $item->name }}" /> @else{!! $item->img !!}@endif</a></figure>
                <h2><a href="{{ $item->link }}">{{ $item->name }}</a></h2>
                <p>Kaina: {{ $item->price  }} Eur @if($item->priceOld>0) <span class="del"><del>{{ $item->priceOld }}</del> Eur</span> @endif</p>

                @if(App\Product::USE_WAREHOUSE && empty($item->qty))
                    <div class="alter alert-danger">Nėra sandėlyje</div>
                @elseif(App\Product::BUY_MODE == 'cart')
                    <a href="{{ action("Frontend\ShoppingCartController@add", [$item->id, $item->CombinationId, 1]) }}" class="ajax-cart-add btn btn-info">Pirkti</a>
                @else
                    <a href="{{ $item->link }}" class="btn btn-info" data-cart-add>Plačiau</a>
                @endif

            </article>
        @endforeach

        {!! $items->appends(['search' => Request::input('search')])->render() !!}
    </div>
@endsection
