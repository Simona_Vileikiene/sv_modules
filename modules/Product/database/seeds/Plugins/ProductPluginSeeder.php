<?php

use Illuminate\Database\Seeder;

class ProductPluginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plugins')->insert([
            'name' => 'Produktai',
            'slug' => 'Product',
            'category_id' => 11
        ]);
        DB::table('categories')->insert([
            'id' => 11,
            'name' => 'Produktai',
            'slug' => 'produktai'
        ]);
    }
}
