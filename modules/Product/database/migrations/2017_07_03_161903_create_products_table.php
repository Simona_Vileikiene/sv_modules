<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('slug')->nullable();
            $table->string('code')->nullable();
            $table->text('body')->nullable();
            $table->integer('priority')->nullable();

            $table->timestamps();
        });

        Schema::create('products_categories', function (Blueprint $table) {
            $table->integer('product_id');
            $table->integer('category_id');

            $table->timestamps();
            $table->index(['category_id', 'product_id']);
        });

        Schema::create('products_images', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('product_id');
            $table->string('photo');
            $table->integer('priority')->nullable();

            $table->timestamps();
        });

        Schema::create('products_combinations', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('product_id');
            $table->string('name')->nullable();
            $table->decimal('price', 10, 2)->nullable();
            $table->decimal('sale', 10, 2)->nullable();
            $table->integer('qty')->default(0);

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
        Schema::drop('products_categories');
        Schema::drop('products_images');
        Schema::drop('products_combinations');
    }
}
