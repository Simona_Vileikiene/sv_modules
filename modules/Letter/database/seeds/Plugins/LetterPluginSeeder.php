<?php

use Illuminate\Database\Seeder;

class LetterPluginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plugins')->insert([
            'name' => 'Prenumerata',
            'slug' => 'Letter',
            'category_id' => 6
        ]);
        DB::table('categories')->insert([
            'id' => 6,
            'name' => 'Laiškų prenumerata',
            'slug' => 'prenumerata'
        ]);
    }
}
