<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Letter extends Model
{
    //no guarded, cause multi lang has unnecessary fields
    protected $fillable = ['name', 'body', 'send_data', 'send_time', 'subscribers_total', 'opened_total', 'sended'];

    static public function siteCategoryId() {
        return \App\Plugin::where('slug', 'letter')->first()->category_id;
    }

    //show send rate
    protected function getSendRateAttribute()
    {
        $users = LettersSubscriber::where('sending_letter_id', $this->id)->count();
        $rate = round($users*100/$this->subscribers_total);
        if(empty($rate)) {
            return 100;
        }
        return $rate;
    }

    //show letter open rate
    protected function getOpenRateAttribute()
    {
        return round($this->opened_total*100/$this->subscribers_total);
    }
}