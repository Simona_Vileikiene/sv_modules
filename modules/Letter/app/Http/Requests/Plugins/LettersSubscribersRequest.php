<?php

namespace App\Http\Requests;

use Request;
use Illuminate\Foundation\Http\FormRequest;

class LettersSubscribersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        //rules if update
        if ($this->method() == 'PATCH') {
            $id = Request::segment(3);
            $rules['email'] = 'required|email|unique:letters_subscribers,email,' . $id;
        } else {
            //rules if create
            $rules['email'] = 'required|email|unique:letters_subscribers,email';
        }
        return $rules;
    }
}