<?php

namespace App\Http\Requests;

use Request;
use Illuminate\Foundation\Http\FormRequest;

class LetterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        $rules = [
            'name' => 'required',
        ];
        if (!empty(Request::input('send_data'))) {
            $rules['send_data'] = 'date_format:Y-m-d';
        }
        if (!empty(Request::input('send_time'))) {
            $rules['send_time'] = 'date_format:H:i:s';
        }
        return $rules;
    }

    public function attributes()
    {
        return [
            'name' => 'Laiško tema',
            'send_data' => 'Siuntimo data',
            'send_time' => 'Siuntimo laikas'
        ];
    }
}