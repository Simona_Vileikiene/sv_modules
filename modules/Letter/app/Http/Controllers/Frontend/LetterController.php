<?php

namespace App\Http\Controllers\Frontend;

use App;
use App\Setting;
use Mail;
use Request;

use App\Category;
use App\Http\Requests\LettersSubscribersSiteRequest;
use App\Letter;
use App\LettersSubscriber;
use App\MultiLang;
use App\LettersOpen;

class LetterController extends App\Http\Controllers\Controller
{

    protected $sendLetters = 1;

    //show subscribe form
    public function index()
    {
        $page = Category::getCurrent();
        if(empty($page)) {
            $page = Category::find(4);
        }
        if (!MultiLang::isDefaultLang()) {
            $page = $page->translate();
        }
        return view('site/pages/letterssubscriber', compact('page'));
    }

    //make subscribtion
    public function subscribe(LettersSubscribersSiteRequest $request)
    {
        $item = LettersSubscriber::where('email', $request->input('email'))->first();
        if(!$item) {
            $item = LettersSubscriber::create($request->all());
        } else {
            $item->update($request->all());
        }
        $item->update(['unsubscribed'=>NULL]);
        return redirect()->action('Frontend\LetterController@index')->
        with('message', Setting::value('letterSubscribe'));
    }

    //unsubscribe
    public function unsubscribe($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $item = LettersSubscriber::where('email', $email)->first();
            if ($item) {
                $item->update(['unsubscribed'=>date('Y-m-d H:i:s')]);
            }
        }
        return redirect()->action('Frontend\LetterController@index')->
        with('message', Setting::value('letterUnsubscribe'));
    }

    //cron send letter
    public function sendLetter()
    {
        $nowData = date('Y-m-d');
        $nowTime = date('H:i').':00';
        $letter = Letter::whereNull ('sended')
            ->where('send_data', '<=', $nowData)->first();
        //if not today or today but earlier
        if(
            $letter &&
            ($letter->send_data<$nowData || ($letter->send_data == $nowData && $letter->send_time<$nowTime))
        ) {
            $letterSubject = $letter->name;
            $letterMessage = $letter->body;
            $unsubscribeLink = Request::root().Category::getUrlByLang(Letter::siteCategoryId()).'/unsubscribe/';
            //begin sending
            if($letter->subscribers_total == 0) {
                $subscribers = LettersSubscriber::whereNull('unsubscribed');
                $letter->update(['subscribers_total' => $subscribers->count()]);
                $subscribers->update(['sending_letter_id' => $letter->id]);
            }

            $items = LettersSubscriber::where('sending_letter_id', $letter->id);
            //finish sending
            if($items->count() == 0) {
                $letter->update(['sended' => 1]);
                exit();
            }
            foreach ($items->limit($this->sendLetters)->get() as $item) {
                $email = $item->email;
                $name = $item->name;
                $openUrl = Request::root() . '/api/letter/open/'.$letter->id.'/'.$email.'?t='.time();
                $openImg = "<img src=" . $openUrl . " height=1 width=1 />";
                $data = [
                    'email' => $email,
                    'letterMessage' => $letterMessage,
                    'unsubscribeLink' => $unsubscribeLink.$email,
                    'openImg' => $openImg
                ];
                if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    Mail::send('emails.letter', $data, function ($message) use ($email, $name, $letterSubject) {
                        $message->to($email, $name)->subject($letterSubject);
                    });
                }
                $item->update(['sending_letter_id' => 0]);
            }
        }
    }

    //counts letter opens
    public function openLetter($letterId = null, $email)
    {
        if(!LettersOpen::where('letter_id', $letterId)->where('email', $email)->first()) {
            LettersOpen::create(['email'=>$email, 'letter_id' => $letterId]);
            $letter = Letter::find($letterId);
            if($letter) {
                $letter->update(['opened_total' => $letter->opened_total+1]);
            }
        }
        header("Content-type: image/png");
        $im = @imagecreate(1, 1) or die("Cannot Initialize new GD image stream");
        imagepng($im);
        imagedestroy($im);
        return null;
    }
}