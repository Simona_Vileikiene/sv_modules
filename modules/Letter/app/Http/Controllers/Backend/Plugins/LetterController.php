<?php 

namespace App\Http\Controllers; 

use Request;
use App\Letter;
use App\Http\Requests\LetterRequest;
use Response;

class LetterController extends Controller
{
    //admin. list
    public function index()
    {
        $items = Letter::orderby("send_data", 'DESC')->orderby("send_time", 'DESC')->paginate(50);
       	return view('admin/letter/index', compact('items'));
    }

    //admin. create new
    public function create($id=0)
    {
        return view('admin/letter/create', compact('id'));
    }

    //admin. save create
    public function store(LetterRequest $request)
    {
        $item = Letter::create($request->all());
        $this->makeSendDataCurrent($item);
        return redirect()->action('LetterController@index'); 
    }

    //admin. edit
    public function edit($id)
    {
        $item = Letter::findorFail($id);
        return view('admin/letter/edit', compact('item'));
    }

    //admin. save edit
    public function update($id, LetterRequest $request)
    {
        $item = Letter::findorFail($id);
        $item->update($request->all());
        $this->makeSendDataCurrent($item);
        return redirect()->action('LetterController@index'); 
    }

    //admin. destroy
    public function destroy($id)
    {
        $item = Letter::findorFail($id);
        $item->delete();
        return redirect()->action('LetterController@index'); 
    }

    //no used, but laravel should has it for resource route
    public function show()
    {
        exit();
    }

    //if it necessary make send data and time current
    protected function makeSendDataCurrent($item) {
        if (empty(Request::input('send_data'))) {
            $item->update(['send_data' => date('Y-m-d')]);
        }
        if (empty(Request::input('send_time'))) {
            $item->update(['send_time' => date('H:i')]);
        }
    }
}
