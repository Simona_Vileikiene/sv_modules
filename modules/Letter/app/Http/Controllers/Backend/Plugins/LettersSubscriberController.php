<?php 

namespace App\Http\Controllers; 

use Request;
use App\LettersSubscriber;
use App\Http\Requests\LettersSubscribersRequest;
use Response;

class LettersSubscriberController extends Controller
{
    //admin. list
    public function index()
    {
        $items = LettersSubscriber::orderby("created_at", 'DESC')->paginate(50);
       	return view('admin/letterssubscriber/index', compact('items'));
    }

    //admin. create new
    public function create($id=0)
    {
        return view('admin/letterssubscriber/create', compact('id'));
    }

    //admin. save create
    public function store(LettersSubscribersRequest $request)
    {
        $item = LettersSubscriber::create($request->all());
        $this->makeUserUnsubscribed($item);
        return redirect()->action('LettersSubscriberController@index');
    }

    //admin. edit
    public function edit($id)
    {
        $item = LettersSubscriber::findorFail($id);
        return view('admin/letterssubscriber/edit', compact('item'));
    }

    //admin. save edit
    public function update($id, LettersSubscribersRequest $request)
    {
        $item = LettersSubscriber::findorFail($id);
        $item->update($request->all());
        $this->makeUserUnsubscribed($item);
        return redirect()->action('LettersSubscriberController@index');
    }

    //admin. destroy
    public function destroy($id)
    {
        $item = LettersSubscriber::findorFail($id);
        $item->delete();
        return redirect()->action('LettersSubscriberController@index');
    }

    //no used, but laravel should has it for resource route
    public function show()
    {
        exit();
    }

    protected function makeUserUnsubscribed($item) {
        if(Request::input('unsubscribed_check')==1 && empty($item->unsubscribed)) {
            $item->update(['unsubscribed'=>date('Y-m-d H:i:s')]);
        } elseif(Request::input('unsubscribed_check') !=1 ) {
            $item->update(['unsubscribed'=>NULL]);
        }
    }
}
