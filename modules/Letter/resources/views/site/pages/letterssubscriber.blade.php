@extends('site.layout')

@section('content')

    <div class="col-md-6 col-md-offset-3">
        <h1>Sukurti naują</h1>
        @if (Session::get('message'))
            <p class="alert alert-success">{{ Session::get('message') }}</p>
        @else
            @include('errors/list')
            {!! Form::open(array('action' => ['Frontend\LetterController@subscribe'], 'files' => true)) !!}
                {!! Form::label('Vardas', '', ['class'=>'control-label']) !!}
                {!! Form::text('name', @$name, ['class'=>'form-control multi']) !!}<br />

                {!! Form::label('El. paštas*', '', ['class'=>'control-label']) !!}
                {!! Form::text('email', @$email, ['class'=>'form-control']) !!}<br />

                {!! Form::submit('Siųsti', ['class'=>'btn btn-info btn-lg']) !!}
            {!! Form::close() !!}
        @endif
    </div>

@endsection