@extends('admin/layout')

@section('content')

	<div class="col-md-6 col-md-offset-3">
		<h1>Sukurti naują</h1>
		@include('errors/list') 
		{!! Form::open(array('action' => ['LetterController@store'], 'files' => true)) !!}
			@include('admin/letter/form')
		{!! Form::close() !!}
	</div>
	
@endsection