

{!! Form::label('Laiško tema*', '', ['class'=>'control-label']) !!}
{!! Form::text('name', @$name, ['class'=>'form-control multi']) !!}<br />

{!! Form::label('Siuntimo data', '', ['class'=>'control-label',
'data-comment'=>' pvz. 2015-11-21; nieko neįrašius - siųsti pradės dabar']) !!}
{!! Form::text('send_data', @$send_data, ['class'=>'form-control datepicker']) !!}<br />

{!! Form::label('Siuntimo laikas', '', ['class'=>'control-label', 'data-comment'=>' pvz. 13:45:00']) !!}
{!! Form::text('send_time', @$send_time, ['class'=>'form-control']) !!}<br />

{!! Form::label('Laiškas', '', ['class'=>'control-label']) !!}
<span class="multi" name="body">{!! Form::textarea('body', @$body, ['class'=>'form-control redactor']) !!}</span><br />

{!! Form::submit('Siųsti', ['class'=>'btn btn-info btn-lg']) !!}
