@extends('admin/layout')

@section('content')

    <h1 class="pull-left">Prenumeratos laiškai</h1>
    <a href="{{ action("LetterController@create") }}" class="btn btn-success pull-right">
        <i class="glyphicon glyphicon-plus"></i> Sukurti naują
    </a>
    <a href="{{ action("LettersSubscriberController@index") }}" class="btn btn-primary pull-right"
       style="margin-right:10px;">
        <i class="glyphicon glyphicon-user"></i> Prenumeratoriai
    </a>
    <div class="clearfix"></div><br/>


    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>#</th>
                <th>Laiško tema</th>
                <th>Siuntimo laikas</th>
                <th>Gavėjų skaičius</th>
                <th>Atidarymo procentas</th>
                <th width="200"></th>
            </tr>
            @foreach($items as $item)

                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->send_data }} {{ $item->send_time }}</td>
                    @if(!empty($item->sended))
                        <td>{{ $item->subscribers_total }}</td>
                        <td>{{ $item->openRate }}% ({{ $item->subscribers_total.'/'.$item->opened_total }})</td>
                    @elseif($item->subscribers_total)
                        <td colspan="2">Siunčiama... {{ $item->sendRate }}%</td>
                    @else
                        <td colspan="2">Dar neišsiųsta</td>
                    @endif
                    <td>
                        <a href="{{ action("LetterController@edit", $item->id) }}" class="btn btn-info"
                           title="Redaguoti"><i class="glyphicon glyphicon-pencil"></i></a>
                        {!! Form::model($item, ['method' => 'DELETE', 'action' => ['LetterController@destroy', $item->id], 'class'=>'del-form', 'onclick'=>"return confirm('Ar tikrai norite ištrinti?')", 'title'=>'Ištrinti']) !!}
                        {!! Form::submit(' X ', ['class'=>'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>

            @endforeach
        </table>
    </div>

    {!! $items->appends(['category' => Request::input('category')])->render() !!}
@endsection 