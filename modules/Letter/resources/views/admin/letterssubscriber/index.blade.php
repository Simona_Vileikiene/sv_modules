@extends('admin/layout')

@section('content')

    <h1 class="pull-left">Prenumeratoriai</h1>
    <a href="{{ action("LettersSubscriberController@create") }}" class="btn btn-success pull-right">
        <i class="glyphicon glyphicon-plus"></i> Sukurti naują
    </a>
    <div class="clearfix"></div><br/>


    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>#</th>
                <th>Vardas</th>
                <th>El. paštas</th>
                <th>Atsisakęs</th>
                <th width="200"></th>
            </tr>
            @foreach($items as $item)

                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->email }}</td>
                    <td>{{ $item->unsubscribed }}</td>
                    <td>
                        <a href="{{ action("LettersSubscriberController@edit", $item->id) }}" class="btn btn-info"
                           title="Redaguoti"><i class="glyphicon glyphicon-pencil"></i></a>
                        {!! Form::model($item, ['method' => 'DELETE', 'action' => ['LettersSubscriberController@destroy', $item->id], 'class'=>'del-form', 'onclick'=>"return confirm('Ar tikrai norite ištrinti?')", 'title'=>'Ištrinti']) !!}
                        {!! Form::submit(' X ', ['class'=>'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>

            @endforeach
        </table>
    </div>

    {!! $items->appends(['category' => Request::input('category')])->render() !!}
@endsection 