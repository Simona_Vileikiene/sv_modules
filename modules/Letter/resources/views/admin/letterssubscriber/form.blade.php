{!! Form::checkbox('unsubscribed_check', 1, @$unsubscribed_check, ['class'=>'form-control radio']) !!}
{!! Form::label('Atsisakęs naujienų', '', ['class'=>'control-label']) !!} <br />

{!! Form::label('Vardas', '', ['class'=>'control-label']) !!}
{!! Form::text('name', @$name, ['class'=>'form-control multi']) !!}<br />

{!! Form::label('El. paštas*', '', ['class'=>'control-label']) !!}
{!! Form::text('email', @$email, ['class'=>'form-control']) !!}<br />



{!! Form::submit('Siųsti', ['class'=>'btn btn-info btn-lg']) !!}
