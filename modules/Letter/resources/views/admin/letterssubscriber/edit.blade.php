@extends('admin.layout')

@section('content')

	<div class="col-md-6 col-md-offset-3">
		<h1>Redaguoti</h1>
		@include('errors/list')
		{!! Form::model($item, ['method' => 'PATCH', 'action' => ['LettersSubscriberController@update', $item->id], 'files' => true]) !!}
			@include('admin/letterssubscriber/form')
        {!! Form::close() !!}
	</div>
	
@endsection