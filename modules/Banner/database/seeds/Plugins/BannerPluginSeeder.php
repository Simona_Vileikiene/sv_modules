<?php

use Illuminate\Database\Seeder;

class BannerPluginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plugins')->insert([
            'name' => 'Banner',
            'slug' => 'Banner'
        ]);

        //test item
        DB::table('banners')->insert([
            'id' => 1,
            'name' => 'Baneris testas',
            'body' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>',
            'link' => 'http://www.beenet.lt',
        ]);

        //multilang
        $multiLang = new App\MultiLang();
        if($multiLang->isMultiLang()) {
            DB::table('trans')->insert([
                'lang' => 'en',
                'item_module' => 'banner',
                'item_id' => DB::table('banners')->max('id'),
                'item_field' => 'name',
                'body' => 'Test banner'
            ]);
            DB::table('trans')->insert([
                'lang' => 'en',
                'item_module' => 'banner',
                'item_id' => DB::table('banners')->max('id'),
                'item_field' => 'body',
                'body' => '<p>EN Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>'
            ]);
        }
    }
}
