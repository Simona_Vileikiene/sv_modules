<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;
use Request;
use App\Image;
use App;

class Banner extends Model
{
    //no guarded, cause multi lang has unnecessary fields
    protected $fillable = ['name', 'body', 'link', 'color'];

    //images gallery path
    const IMAGES_PATH = '/images/banner/';

    //get img html atrribute [$item->img]
    protected function getImgAttribute()
    {
        return Image::getImgHtml($this->photo, Banner::IMAGES_PATH, $this->name);
    }

    /**
     * Prepare item translation
     *
     * @param TranHelper|null $tranHelper
     * @return Category
     */
    public function translate($tranHelper = null)
    {
        $tranHelper = ($tranHelper) ? $tranHelper : new TranHelper();
        return $tranHelper->getTranslatedItem($this, 'banner');
    }
}
