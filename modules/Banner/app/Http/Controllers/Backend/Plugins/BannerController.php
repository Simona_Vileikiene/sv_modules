<?php 

namespace App\Http\Controllers; 

use App\Priority;
use App\TranHelper;
use Request;
use App\Banner;
use App\Tran;
use App\Image;
use Illuminate\Validation\Validator;
use App\Http\Requests\BannerRequest;
use Response;

use Illuminate\Support\Str;

class BannerController extends Controller 
{
    public function __construct()
    {
        $this->tranHelper = new TranHelper();
    }

    /**
     * Admin. Banners list
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if(Request::input('sortable')) {
            $items = Banner::orderby("priority")->paginate(9999);
        } else {
            $items = Banner::orderby("priority")->paginate(50);
        }
       	return view('admin/banner/index', compact("items"));
    }

    /**
     * Admin. create new
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($id=0)
    {
        return view('admin/banner/create', compact('id'));
    }

    /**
     * Admin. save new
     *
     * @param BannerRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(BannerRequest $request)
    {
        $item = Banner::create($request->all());
        $priority = new Priority(Banner::whereNotNull('id'), $item);
        $priority->addPriority();
        //upload new image
        if($request->file('photo')) {
            $imageName = Image::uploadImage($item, $request->file('photo'), Banner::IMAGES_PATH);
            $item->photo = $imageName;
            $item->save();
        }
        //if multi lang save values
        $this->tranHelper->setItemTranslations($item->id, Request::segment(2), $request);
        return redirect()->action('BannerController@index'); 
    }

    /**
     * Admin. edit
     *
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $item = Banner::findorFail($id);
        return view('admin/banner/edit', compact('item'));
    }

    /**
     * Admin. save edit
     *
     * @param $id
     * @param BannerRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, BannerRequest $request)
    {
        $item = Banner::findorFail($id);
        $item->update($request->all());
        //delete old and upload new image
        if($request->file('photo')) {
            Image::removeImage($item->photo, Banner::IMAGES_PATH);
            $imageName = Image::uploadImage($item, $request->file('photo'), Banner::IMAGES_PATH);
            $item->photo = $imageName;
            $item->save();
        }
        //if multi lang save values
        $this->tranHelper->setItemTranslations($item->id, Request::segment(2), $request);
        return redirect()->action('BannerController@index'); 
    }

    /**
     * Admin. destroy
     *
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $item = Banner::findorFail($id);
        Image::removeImage($item->photo, Banner::IMAGES_PATH);
        //if multi lang remove values
        $this->tranHelper->removeItemTranslations($item->id, Request::segment(2));
        $item->delete();
        //check weight
        $priority = new Priority(Banner::whereNotNull('id'), $item);
        $priority->fixPriorities();
        return redirect()->action('BannerController@index'); 
    }

    //no used, but laravel should has it for resource route
    public function show()
    {
        exit();
    }

}
