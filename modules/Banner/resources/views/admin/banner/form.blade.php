{!! Form::label('Nuotrauka', '', ['class'=>'control-label']) !!}
@if(isset($item) and $item->img!="")
    <span style="width:100px;display:inline-block" class="thumb">{!! $item->img !!}</span>
@endif
{!! Form::file('photo', ['class'=>'form-control']) !!}<br />

<div class="colorPick">
    {!! Form::label('', 'Pavadinimo spalva*', ['class'=>'control-label']) !!}
    <div style="background-color: {{ old('color', (isset($item))?$item->color:'#000000') }};"></div>
    {!! Form::text('color', old('color', (isset($item))?$item->color:'#000000'), ['readonly' => 'true']) !!}
</div>

{!! Form::label('Pavadinimas*', '', ['class'=>'control-label']) !!}
{!! Form::text('name', @$name, ['class'=>'form-control multi']) !!}<br />

{!! Form::label('Tekstas', '', ['class'=>'control-label']) !!}
<span class="multi" name="body">{!! Form::textarea('body', @$body, ['class'=>'form-control redactor']) !!}</span><br />

{!! Form::label('Nuoroda', '', ['class'=>'control-label']) !!}
{!! Form::text('link', @$link, ['class'=>'form-control multi']) !!}<br />

{!! Form::submit('Siųsti', ['class'=>'btn btn-outline-main  btn-lg']) !!}
