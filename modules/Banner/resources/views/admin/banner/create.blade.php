@extends('admin.layout')

@section('content')

    <div class="module-form">
        <h1>{{ trans('admin.create_new') }}</h1>
		@include('errors/list')
		{!! Form::open(array('action' => ['BannerController@store'], 'files' => true)) !!}
			@include('admin/banner/form')
		{!! Form::close() !!}
	</div>

@endsection
