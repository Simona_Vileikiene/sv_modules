@extends('admin.layout')

@section('content')

    <div class="module-form">
        <h1>{{ trans('admin.edit') }}</h1>
		@include('errors/list')
		{!! Form::model($item, ['method' => 'PATCH', 'action' => ['BannerController@update', $item->id], 'files' => true]) !!}
			@include('admin/banner/form')
        {!! Form::close() !!}
	</div>

@endsection
