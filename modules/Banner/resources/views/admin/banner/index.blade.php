@extends('admin/layout')

@section('content')

    <div class="row">
        <h1 class="col-lg-9 text-lg-left text-center">{{ __('Baneriai') }}</h1>
        <div class="col-lg-3 text-right p-2">
            <a href="{{ action("BannerController@create") }}" class="btn btn-outline-success">
                <i class="fas fa-plus"></i> {{ trans('admin.create_new') }}
            </a>
        </div>
    </div>

	<div class="table-responsive">
		<table class="table table-hover actions-2" @if(Request::input('sortable')) data-sortable="\App\Banner" @endif>
			<tr @if(Request::input('sortable')) class="sortable-disable" @endif>
			<tr>
				<th>#</th>
				<th>Nuotrauka</th>
				<th>Pavadinimas</th>
				<th>Nuoroda</th>
                <th width="150">
                    <a href="{{ action("BannerController@index") }}{{ (Request::input('sortable'))?'':'?sortable=1' }}"
                       class="btn btn-sm btn-outline-warning text-nowrap"
                       title="{{ (Request::input('sortable'))?'Sustabdyti':'Rikiuoti' }}">
                        <i class="fas fa-sort"></i> {{ (Request::input('sortable'))?'Sustabdyti':'Rikiuoti' }}
                    </a>
                </th>
			</tr>
			@foreach($items as $item)

				<tr data-sortable-id="{{ $item->id }}">
					<td>{{ $item->id }}</td>
					<td class="thumb">{!! $item->img !!}</td>
					<td>{{ $item->name }}</td>
					<td><a href="{{ $item->link }}" target="_blank">{{ $item->link }}</a></td>
					<td>
						<a href="{{ action("BannerController@edit", $item->id) }}" class="btn btn-sm btn-outline-info" title="Redaguoti"><i class="far fa-edit"></i></a>
                        {!! Form::model($item, ['method' => 'DELETE', 'action' => ['BannerController@destroy', $item->id], 'class'=>'d-inline', 'onclick'=>"return confirm('Ar tikrai norite ištrinti?')", 'title'=>'Ištrinti']) !!}
                        {!! Form::submit(' X ', ['class'=>'btn btn-sm btn-outline-danger']) !!}
                        {!! Form::close() !!}
                        @if(Request::input('sortable'))
                            <span class="btn btn-sm"><i class="fas fa-arrows-alt"></i></span>
                        @endif
					</td>
				</tr>

			@endforeach
		</table>
	</div>

	{!! $items->appends(['search' => Request::input('search')])->render() !!}
@endsection
