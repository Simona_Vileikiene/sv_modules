<div id="carouselExampleIndicators" class="carousel slide container" data-ride="carousel">
	<div class="carousel-inner" role="listbox">
		@foreach($homeBanners as $k=>$item)
			<article
					class="@if($k==0) active @endif carousel-item"
					@if(!empty($item->link)) onclick="return window.location='{{ $item->link }}'" @endif
			>
				<h3 style="color: {{ $item->color }}">{{ $item->translate()->name }}</h3>
				{!! $item->translate()->body !!}
				<figure class="thumbnail">{!! $item->img !!}</figure>
			</article>
		@endforeach
	</div>

	<ol class="carousel-indicators">
		@foreach($homeBanners as $k=>$item)
			<li data-target="#carouselExampleIndicators" data-slide-to="{{ $k }}" class="@if($k==0) active @endif"></li>
		@endforeach
	</ol>
	<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
		<span class="carousel-control-next-icon" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
</div>
