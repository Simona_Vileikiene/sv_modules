<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use File;

class FeaturesCategory extends Model
{
	protected $fillable  = ['category_id', 'feature_id'];
    protected $table = 'categories_features';

    //RALATIONSHIPS. each img has one product
	public function feature()
    {
        return $this->belongsTo('App\Feature');
    }

    //RALATIONSHIPS. each img has one product
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    static function manageFeaturesCategories($item, $request = NULL) {
        $ids_from_request = [];
        if(!empty($request) && is_array($request->input('categories'))) {
            $ids_from_request = collect(array_map(function ($a) {
                return ['category_id' => (int)$a];
            }, $request->get('categories')))->keyBy('category_id');
        }
        $ids_from_item = $item->categories->keyBy('category_id');
        $ids_to_delete = $ids_from_item->diffKeys($ids_from_request)->pluck('category_id')->all();
        $item->categories()->whereIn('category_id', $ids_to_delete)->delete();

        if(!empty($request) && is_array($request->input('categories'))) {
            foreach ($request->input('categories') as $one) {
                FeaturesCategory::updateOrCreate(['feature_id' =>$item->id, 'category_id' => $one]);
            }
        }
    }
}
