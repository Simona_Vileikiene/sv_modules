<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use File;

class FeaturesOption extends Model
{
	protected $fillable  = ['feature_id', 'name'];

    //RALATIONSHIPS. each img has one product
	public function feature()
    {
        return $this->belongsTo('App\Feature');
    }

    //manage product categories
    static function manageFeaturesOptions($item, $request = NULL) {
        $ids_from_request = [];
        if(!empty($request) && is_array($request->input('options_ids'))) {
            $ids_from_request = collect(array_map(function ($a) {
                return ['id' => (int)$a];
            }, $request->get('options_ids')))->keyBy('id');
        }
        $ids_from_item = $item->options->keyBy('id');
        $ids_to_delete = $ids_from_item->diffKeys($ids_from_request)->pluck('id')->all();
        $item->options()->whereIn('id', $ids_to_delete)->delete();

        if(!empty($request) && is_array($request->input('options'))) {
            foreach ($request->input('options') as $id => $one) {
                if(!empty($one)) {
                    $item2 = FeaturesOption::firstOrCreate(['feature_id' => $item->id, 'id' => $id]);
                    $item2->update(['name' => $one]);
                }
            }
        }
    }
}
