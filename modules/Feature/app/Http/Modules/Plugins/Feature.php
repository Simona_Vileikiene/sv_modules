<?php

namespace App;

use App;
use Request;
use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{

    const CATEGORY_ID = 7;

    //no guarded, cause multi lang has unnecessary fields
    protected $fillable = ['name', 'required', 'show_on_catalog'];

    public function options()
    {
        return $this->hasMany('App\FeaturesOption');
    }

    public function categories()
    {
        return $this->hasMany('App\FeaturesCategory');
    }

    protected function getCategoriesListAttribute() {
        $categories = $this->categories()->pluck('category_id')->toArray();
        return Category::whereIn('id', $categories);
    }

    protected function getOptionsSelectAttribute() {
        $multiLang = new MultiLang();
        $items = [];
        foreach ($this->options as $option) {
            if($multiLang->isDefaultLang()) {
                $items[$option->id] = $option->name;
            } else {
                $nn = 'options_'.$option->id;
                $items[$option->id] = $this->translate()->$nn;
            }
        }
        $items[0] = ' '.$this->name;
        asort($items);
        return $items;
    }

    public function translate($tranHelper = null)
    {
        $tranHelper = ($tranHelper) ? $tranHelper : new TranHelper();
        return $tranHelper->getTranslatedItem($this, 'feature');
    }
}