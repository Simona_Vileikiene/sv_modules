<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use File;

class FeaturesProduct extends Model
{
	protected $fillable  = ['features_option_id', 'product_id'];
    protected $table = 'products_features';

    //RALATIONSHIPS. each img has one product
	public function feature()
    {
        return $this->belongsTo('App\FeaturesOption', 'features_option_id');
    }

    //RALATIONSHIPS. each img has one product
    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    static function manageFeaturesProduct($item, $request = NULL) {
        $ids_from_request = [];
        if(!empty($request) && is_array($request->input('features'))) {
            $ids_from_request = collect(array_map(function ($a) {
                return ['features_option_id' => (int)$a];
            }, $request->get('features')))->keyBy('features_option_id');
        }
        $ids_from_item = $item->features->keyBy('features_option_id');
        $ids_to_delete = $ids_from_item->diffKeys($ids_from_request)->pluck('features_option_id')->all();
        $item->features()->whereIn('features_option_id', $ids_to_delete)->delete();
        if(!empty($request) && is_array($request->input('features'))) {
            foreach ($request->input('features') as $k=>$one) {
                if($one > 0) {
                    FeaturesProduct::updateOrCreate(['product_id' => $item->id, 'features_option_id' => $one]);
                }
            }
        }
    }
}
