<?php 

namespace App\Http\Controllers; 

use App\CategoryHelper;
use App\FeaturesCategory;
use App\FeaturesOption;
use App\TranHelper;
use Request;
use App\Feature;
use App\Http\Requests\FeatureRequest;
use Response;

class FeatureController extends Controller 
{
    public function __construct()
    {
        $this->categoryHelper = new CategoryHelper();
        $this->tranHelper = new TranHelper();
    }

    //admin. list
    public function index()
    {
        $items = Feature::orderby("name")->paginate(50);
       	return view('admin/feature/index', compact("items"));
    }

    //admin. create new
    public function create($id=0)
    {
        $categories = $this->categoryHelper->getAllSubcategoriesList(Feature::CATEGORY_ID);
        return view('admin/feature/create', compact('id', 'categories'));
    }

    //admin. save create
    public function store(FeatureRequest $request)
    {
        $item = Feature::create($request->all());
        $item->update(["required" => ($request->input("required")==1)?1:0]);
        $item->update(["show_on_catalog" => ($request->input("show_on_catalog")==1)?1:0]);
        FeaturesCategory::manageFeaturesCategories($item, $request);
        FeaturesOption::manageFeaturesOptions($item, $request);
        //if multi lang save values
        $this->tranHelper->setItemTranslations($item->id, Request::segment(2), $request);
        return redirect()->action('FeatureController@index');
    }

    //admin. edit
    public function edit($id)
    {
        $item = Feature::findorFail($id);
        $categories = $this->categoryHelper->getAllSubcategoriesList(Feature::CATEGORY_ID);
        return view('admin/feature/edit', compact('item', 'categories'));
    }

    //admin. save edit
    public function update($id, FeatureRequest $request)
    {
        $item = Feature::findorFail($id);
        $item->update($request->all());
        $item->update(["required" => ($request->input("required")==1)?1:0]);
        $item->update(["show_on_catalog" => ($request->input("show_on_catalog")==1)?1:0]);
        FeaturesCategory::manageFeaturesCategories($item, $request);
        FeaturesOption::manageFeaturesOptions($item, $request);
        //if multi lang save values
        $this->tranHelper->setItemTranslations($item->id, Request::segment(2), $request);
        return redirect()->action('FeatureController@index');
    }

    //admin. destroy
    public function destroy($id)
    {
        $item = feature::findorFail($id);
        FeaturesCategory::manageFeaturesCategories($item);
        FeaturesOption::manageFeaturesOptions($item);
        $this->tranHelper->removeItemTranslations($item->id, Request::segment(2));
        $item->delete();
        return redirect()->action('FeatureController@index');
    }

    //no used, but laravel should has it for resource route
    public function show()
    {
        exit();
    }

}
