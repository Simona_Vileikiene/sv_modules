{!! Form::checkbox('required', 1, @$required, ['class'=>'radio']) !!} Privalomas kuriant/redaguojant produktą<br/><br/>
{!! Form::checkbox('show_on_catalog', 1, @$show_on_catalog, ['class'=>'radio']) !!} Rodyti kataloge<br/><br/>

{!! Form::label('Pavadinimas*', '', ['class'=>'control-label']) !!}
{!! Form::text('name', @$name, ['class'=>'form-control multi']) !!}<br />

<div data-options>
    <h4>Pasirinkimai</h4>
    @if(isset($item))
        @foreach($item->options as $option)
            <div class="option">
                <div class="input-group">
                    {!! Form::hidden('options_ids[]', $option->id) !!}
                    {!! Form::text('options['.$option->id.']', $option->name, ['class'=>'form-control multi', 'placeholder' => 'Pasirinkimo pavadinimas']) !!}
                    <span class="input-group-btn">
                        <button class="btn btn-danger btn-xs remove_item" type="button">
                            <i class="fa fa-minus"></i>
                        </button>
                    </span>
                </div>
            </div>
        @endforeach
    @endif
    <div class="option">
        <div class="input-group">
            {!! Form::text('options[]', '', ['class'=>'form-control multi', 'placeholder' => 'Pasirinkimo pavadinimas']) !!}
            <span class="input-group-btn">
                <button class="btn btn-danger btn-xs remove_item" type="button">
                    <i class="fa fa-minus"></i>
                </button>
            </span>
        </div>
    </div>
</div>
<span data-options-add class="btn btn-outline-success btn-sm float-right">
    <span class="glyphicon glyphicon-plus"></span>
    Pridėti naują pasirinkimą
</span><br />

<br />
@if(count($categories)>2)
    {!! Form::label('Kategorijos*', '', ['class'=>'control-label']) !!}
    {!! Form::select('categories[]', $categories, (isset($item))?$item->categories()->pluck('category_id'):[], ['class'=>'form-control', 'multiple'=>'multiple' , 'size'=>10]) !!}
    <br/>
@else
    {!! Form::hidden('categories[]', App\Product::siteCategoryId(), ['class'=>'form-control']) !!}
@endif

{!! Form::submit('Siųsti', ['class'=>'btn btn-outline-main  btn-lg']) !!}

@section('footer')
<script>
    /**
    * Survey
    */
    $('[data-options-add]').on('click', addItem);
    $(document).on('click', '.remove_item', removeItem);

    function addItem() {
        var item = $('[data-options] div').last().clone();
        $(item).find('input, select').val('');
        $('[data-options]').append(item);
        //reorderItems();
    }

    function removeItem() {
        $(this).closest('div.option').remove();
        //reorderItems();
    }

    function reorderItems() {
        $("[data-options] div").each(function (index, el) {
            $(this).find('input, select').each(function () {
                $(this).attr('name', function (i, html) {
                    return $(this).attr('name').replace(/options\[(.+?)\]/g, 'options[' + (index) + ']');
                });
            });
        });
    }
</script>
@endsection
