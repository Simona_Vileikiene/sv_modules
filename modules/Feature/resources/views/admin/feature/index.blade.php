@extends('admin/layout')

@section('content')

    <div class="row">
        <h1 class="col-lg-9 text-lg-left text-center">{{ __('Filtrai') }}</h1>
        <div class="col-lg-3 text-right p-2">
            <a href="{{ action("FeatureController@create") }}" class="btn btn-outline-success">
                <i class="fas fa-plus"></i> {{ trans('admin.create_new') }}
            </a>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-hover actions-2">
            <tr>
            <tr>
                <th>#</th>
                <th>Pavadinimas</th>
                <th>Pasirinkimai</th>
                <th>Kategorijos</th>
                <th>Rodyti kataloge</th>
                <th>Privalomas</th>
                <th width="100"></th>
            </tr>
            @foreach($items as $item)

                <tr data-sortable-id="{{ $item->id }}">
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ implode(', ', $item->options()->pluck('name')->toArray()) }}</td>
                    <td>{{ implode(', ', $item->categoriesList->pluck('name')->toArray()) }}</td>
                    <td>{{ ($item->show_on_catalog==1)?'Taip':'' }}</td>
                    <td>{{ ($item->required==1)?'Taip':'' }}</td>
                    <td>
                        <a href="{{ action("FeatureController@edit", $item->id) }}" class="btn btn-sm btn-outline-info"
                           title="Redaguoti"><i class="far fa-edit"></i></a>
                        {!! Form::model($item, ['method' => 'DELETE', 'action' => ['FeatureController@destroy', $item->id], 'class'=>'d-inline', 'onclick'=>"return confirm('Ar tikrai norite ištrinti?')", 'title'=>'Ištrinti']) !!}
                        {!! Form::submit(' X ', ['class'=>'btn btn-sm btn-outline-danger']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>

            @endforeach
        </table>
    </div>
    {!! $items->appends(['search' => Request::input('search')])->render() !!}
@endsection
