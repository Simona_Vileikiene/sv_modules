<?php

use Illuminate\Database\Seeder;

class FeaturePluginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plugins')->insert([
            'name' => 'Filtrai',
            'slug' => 'Feature',
            'category_id' => NULL
        ]);
    }
}
