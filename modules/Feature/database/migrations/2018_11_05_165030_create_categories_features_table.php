<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('features', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name')->nullable();
            $table->smallInteger('required')->nullable();
            $table->smallInteger('show_on_catalog')->nullable();

            $table->timestamps();
        });

        Schema::create('features_options', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('feature_id');
            $table->string('name')->nullable();

            $table->timestamps();
        });

        Schema::create('categories_features', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('category_id');
            $table->integer('feature_id');

            $table->timestamps();
        });

        Schema::create('products_features', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('product_id');
            $table->integer('features_option_id');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('features');
        Schema::dropIfExists('features_options');
        Schema::dropIfExists('categories_features');
        Schema::dropIfExists('products_features');
    }
}
