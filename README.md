# SV modules
SV modules for Laravel 5.6.

## Installation
First require package with composer:
```sh
$ composer require beenet/sv-modules
```
Add service provider to config/app.php:
```php
'providers' => [
    ...
    Beenet\SvModules\SvModulesServiceProvider::class,
],
```
Publish command:
```sh
$ php artisan vendor:publish --provider="Beenet\SvModules\SvModulesServiceProvider" 
```

## Use
Install module:
```sh
$ php artisan sv-module:install [moduleName]
$ php artisan migrate
$ composer dump-autoload
```