<?php

namespace Beenet\SvModules;

use Illuminate\Support\ServiceProvider;

class SvModulesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/SvModuleInstall.php' => app_path('/Console/Commands/').'SvModuleInstall.php'
        ]);
    }

    /**
     * Register the application services.
     *
     *
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('svmodules', function() {
            return new SvModulesManager();
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['svmodules'];
    }
}
