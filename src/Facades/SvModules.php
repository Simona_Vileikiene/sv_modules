<?php

namespace Beenet\SvModules\Facades;

use Illuminate\Support\Facades\Facade;

class SvModules extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'svmodules';
    }
}
