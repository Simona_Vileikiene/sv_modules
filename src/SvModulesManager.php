<?php

namespace Beenet\SvModules;

use Hashids\Hashids;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\URL;
use WebToPay;

class SvModulesManager
{

    private static $instance;

    /**
     * Create instant of self or return already created instant
     *
     * @param array|null $config
     */
    public static function getInstance($config = null)
    {
        return static::$instance ?: (static::$instance = new self($config));
    }

}