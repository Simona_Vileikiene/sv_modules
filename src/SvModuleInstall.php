<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SvModuleInstall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sv-module:install {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install Beenet modules';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $module = $this->argument('name');
        $modulePath = base_path('vendor/beenet/sv-modules/modules/') . $module;
        if (!file_exists($modulePath)) {
            $this->error("Module $modulePath don't exist");
        } else {
            $files = $this->getDirContents($modulePath);
            $this->copyFiles($files, TRUE);
            $installed = $this->copyFiles($files);
            if($installed) {
                $this->info("Module $module copied successfully");
            }
        }
    }

    private function getDirContents($dir, &$results = array())
    {
        $files = scandir($dir);
        foreach ($files as $key => $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (!is_dir($path)) {
                if ($value != 'info.txt') {
                    $results[] = ['name' => $value, 'path' => $path, 'type' => 1];
                }
            } else if ($value != "." && $value != "..") {
                $this->getDirContents($path, $results);
                $results[] = ['name' => $value, 'path' => $path, 'type' => 0];
            }

        }
        return $results;
    }

    private function copyFiles($files, $onlyCategories = FALSE)
    {
        $module = $this->argument('name');
        foreach ($files as $file) {
            $pathInfo = (string)str_replace("\\vendor\beenet\sv-modules\modules\\" . $module, "", $file['path']);
            if ($onlyCategories && $file['type'] == 0) {

                if (!file_exists($pathInfo)) {
                    mkdir($pathInfo);
                }
            }
            if(!$onlyCategories && $file['type'] == 1) {
                if (!file_exists($pathInfo)) {
                    copy($file['path'], $pathInfo);
                } else {
                    $this->error("Module $module is already installed");
                    break;
                    return FALSE;
                }
            }
        }
        return TRUE;
    }
}
